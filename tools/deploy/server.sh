# Pull code
cd /var/www/
git checkout master
git pull origin master

# Build and deploy
yarn install
yarn workspace api run db:sync
yarn run build
pm2 delete bde
pm2 start yarn --name bde -- start:prod