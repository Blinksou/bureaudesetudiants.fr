# bureaudesetudiants.fr

Monorepo of our app "Bureau des étudiants"

## What's inside?

This repo uses [TurboRepo](https://turborepo.org/) for its monorepo architecture and [Yarn](https://classic.yarnpkg.com/en/) as a package manager.

### Apps and Packages

- `api`: a [Nest.js](https://nestjs.com) app running on port 3001
- `web`: another [Next.js](https://nextjs.org) app running on port 3000
- `config`: `eslint` configurations (includes `eslint-config-next` and `eslint-config-prettier`)
- `tsconfig`: `tsconfig.json` used throughout the monorepo

Each package/app is 100% [TypeScript](https://www.typescriptlang.org/).

### Utilities

This repo has some additional tools already setup:

- [TypeScript](https://www.typescriptlang.org/) for static type checking
- [ESLint](https://eslint.org/) for code linting
- [Prettier](https://prettier.io) for code formatting

## Setup

This project works with NodeJS v16, if you don't have it

```bash
nvm install 16
nvm use 16
```

To set up the project

```bash
yarn install
docker-compose up -d
yarn workspace api run db:sync
yarn dev
```

#### Launch commands on apps or packages

If you want to launch yarn commands on apps or packages, you should use this syntax :

```bash
yarn workspace [FOLDER_NAME] [ADD|RUN] [ARGS]

# examples
yarn workspace api add @nest/swagger # add a package
yarn workspace web run test # run a package.json configured script
```

### Build

To build all apps and packages, run the following command:

```
yarn build
```

### Develop

To develop all apps and packages, run the following command:

```
yarn dev
```

## Members

- Marvyn Aboulicam
- Alex Boisseau
- Julien Castera
- Thomas Le Naour
