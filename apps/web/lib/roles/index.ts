import { AssociationsUsers } from "prisma-types";

export const ASSOCIATIONS_ROLES = [
  {
    name: "Membre",
    value: "MEMBER",
  },
  {
    name: "Commercial",
    value: "SALESMAN",
  },
  {
    name: "Responsable des évènements",
    value: "EVENT_MANAGER",
  },
  {
    name: "Manager de la communauté",
    value: "COMMUNITY_MANAGER",
  },
  {
    name: "Trésorier",
    value: "TREASURER",
  },
  {
    name: "Président",
    value: "PRESIDENT",
  },
  {
    name: "Administrateur",
    value: "SCHOOL_ADMIN",
  },
];

export function getAssociationRoleFromAssociationUser(user: AssociationsUsers) {
  let formattedRole = "MEMBRE";
  ASSOCIATIONS_ROLES.forEach((role) => {
    if (role.value === user.role) {
      formattedRole = role.name;
    }
  });

  return formattedRole;
}
