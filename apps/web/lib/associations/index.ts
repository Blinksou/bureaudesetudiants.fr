export function formatAssociationStatus(status: string) {
  switch (status) {
    case "WAITING_APPROVAL":
      return "En attente de validation";
    case "REFUSED":
      return "Refusée";
    case "VALIDATE":
      return "Validée";
    default:
      return "";
  }
}

export function getAssociationStatusBadgeType(status: string) {
  switch (status) {
    case "WAITING_APPROVAL":
      return "warning";
    case "REFUSED":
      return "danger";
    case "VALIDATE":
      return "success";
    default:
      return "success";
  }
}
