import { Association, AssociationsUsers } from "prisma-types";
import { ReactElement, useContext, useState } from "react";
import toast from "react-hot-toast";
import { api } from "../../components/common/api";
import { AuthContext } from "../../contexts/authContext";
import AdminAssociationLayout from "../associations/_layout";
import { NextPageWithLayout } from "../_app";

interface PageProps {
  associations: Array<Association & { members: AssociationsUsers[] }>;
}

enum RequestType {
  JOIN,
  QUIT,
}

const userIsInAssociation = (members: AssociationsUsers[], userId: string) => {
  return members.filter((member) => member.userId === userId).length >= 1;
};

const JoinAnAssociation: NextPageWithLayout<PageProps> = ({ associations }) => {
  const { userId, token, email } = useContext(AuthContext);

  const [filteredAssociations, setFilteredAssociations] =
    useState<typeof associations>(associations);

  const sendRequest = async (
    type: RequestType,
    association: Association,
    userId: string
  ) => {
    const requestToJoin = type === RequestType.JOIN;
    try {
      await api[requestToJoin ? "post" : "delete"](
        `associations/${association.id}/users${
          requestToJoin ? "" : `/${userId}`
        }`,
        {
          ...(requestToJoin && { email }),
        },
        {
          headers: {
            Authorization: "Bearer " + token,
          },
        }
      );

      toast.success(
        `${
          requestToJoin
            ? `Vous faites désormais parti de l\'association ${association.name}`
            : `Vous ne faites désormais plus parti de l'association ${association.name}`
        }`
      );
    } catch (error) {
      toast.error("Une erreur est survenue, veuillez réessayer ...");
    }

    const associations = filteredAssociations.map((asso) => {
      if (asso.id === association.id) {
        if (requestToJoin) {
          asso.members.push({
            userId,
            createdAt: new Date(),
            updatedAt: new Date(),
            role: "MEMBER",
            associationId: association.id,
          });
        } else {
          asso.members = asso.members.filter(
            (member) => member.userId !== userId
          );
        }
      }

      return asso;
    });

    setFilteredAssociations(associations);
  };

  return (
    <main className="flex-1 py-6">
      <h1 className="text-2xl font-semibold text-gray-900">
        Rejoindre une association
      </h1>
      <div className="px-4 sm:px-6 md:px-0 py-10">
        <ul
          role="list"
          className="grid grid-cols-1 gap-6 sm:grid-cols-2 lg:grid-cols-3"
        >
          {filteredAssociations.map((association) => {
            const userIsInCurrentAssociation = userIsInAssociation(
              association.members,
              userId
            );
            return (
              <li
                key={association.id}
                className="col-span-1 bg-white rounded-lg shadow divide-y divide-gray-200 shadow-md border"
              >
                <div className="w-full flex items-center justify-between p-6 space-x-6">
                  <div className="flex-1 truncate">
                    <div className="flex items-center space-x-3">
                      <h3 className="text-gray-900 text-sm font-medium truncate">
                        {association.name}
                      </h3>
                    </div>
                    <p className="mt-1 text-gray-500 text-sm truncate">
                      {association.description}
                    </p>
                    <p className="mt-1 text-gray-500 text-sm truncate">
                      {association.members.length} membres
                    </p>
                  </div>
                </div>
                <div>
                  <div className="-mt-px flex divide-x divide-gray-200">
                    <button
                      onClick={() =>
                        sendRequest(
                          userIsInCurrentAssociation
                            ? RequestType.QUIT
                            : RequestType.JOIN,
                          association,
                          userId
                        )
                      }
                      className={`relative -mr-px w-0 flex-1 inline-flex items-center justify-center py-4 text-sm ${
                        userIsInCurrentAssociation
                          ? "text-red-700"
                          : "text-green-700"
                      } font-medium border border-transparent rounded-bl-lg ${
                        userIsInCurrentAssociation
                          ? "hover:bg-red-100"
                          : "hover:bg-green-100"
                      }`}
                    >
                      <span className="ml-3">
                        {userIsInCurrentAssociation ? "Quitter" : "Rejoindre"}
                      </span>
                    </button>
                  </div>
                </div>
              </li>
            );
          })}
        </ul>
      </div>
    </main>
  );
};

export const getServerSideProps = async (context: any) => {
  const { data } = await api.get(`/associations/?status=VALIDATE`, {
    headers: {
      Authorization: "Bearer " + context.req.cookies["user.token"],
    },
  });

  const associations = await Promise.all(
    data.map(async (association: Association) => {
      const { data } = await api.get(`associations/${association.id}/users`, {
        headers: {
          Authorization: "Bearer " + context.req.cookies["user.token"],
        },
      });

      return {
        ...association,
        members: data,
      };
    })
  );

  return {
    props: {
      associations,
    },
  };
};

JoinAnAssociation.getLayout = function getLayout(page: ReactElement) {
  return <AdminAssociationLayout>{page}</AdminAssociationLayout>;
};

export default JoinAnAssociation;
