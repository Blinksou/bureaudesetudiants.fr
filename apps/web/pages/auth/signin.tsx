import axios from "axios";
import { useRouter } from "next/router";
import { FC, useContext } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import toast from "react-hot-toast";
import Button from "../../components/common/Button";
import { AuthContext } from "../../contexts/authContext";
import Link from "next/link";

type FormValues = {
  email: string;
  password: string;
};

const SignIn: FC = (): JSX.Element => {
  const auth = useContext(AuthContext);
  const router = useRouter();

  if (auth.isLoggedIn) router.push("/associations");

  const { register, handleSubmit } = useForm();
  const onSubmit: SubmitHandler<FormValues> = async (formData: any) => {
    try {
      const { data } = await axios.post("/api/signin", {
        email: formData.email,
        password: formData.password,
      });

      const { token } = data.data;
      const { id, email, isAdmin, profile, updatedAt } = data.data.data;

      auth.login({
        userId: id,
        email,
        isAdmin,
        profile,
        token: token,
        updatedAt,
        isLoggedIn: auth.isLoggedIn,
      });

      router.push(`/${isAdmin ? "/admin" : "/associations"}`);
    } catch (error) {
      toast.error(
        "Erreur lors de la connexion. Veuillez renseigner vos informations."
      );
    }
  };

  return (
    <>
      <div className="flex-1 flex flex-col items-center justify-center py-2 px-4 sm:px-6">
        <div className="mx-auto w-full max-w-sm">
          <div>
            <h2 className="mt-6 text-3xl font-extrabold text-gray-900">
              Connexion
            </h2>
            <p className="mt-2 text-sm text-gray-600">
              Pas de compte ?
              <Link href="/auth/register">
                <a className="font-medium text-secondary hover:text-sky-700">
                  {"Je m'inscris"}
                </a>
              </Link>
            </p>
          </div>

          <div className="mt-8">
            <div className="mt-6">
              <form onSubmit={handleSubmit(onSubmit)} className="space-y-6">
                <div>
                  <label
                    htmlFor="email"
                    className="block text-sm font-medium text-gray-700"
                  >
                    Adresse email
                  </label>
                  <div className="mt-1">
                    <input
                      {...register("email", { required: true })}
                      id="email"
                      name="email"
                      type="email"
                      autoComplete="email"
                      required
                      className="w-full appearance-none px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-primary focus:border-primary sm:text-sm"
                    />
                  </div>
                </div>

                <div className="space-y-1">
                  <label
                    htmlFor="password"
                    className="block text-sm font-medium text-gray-700"
                  >
                    Mot de passe
                  </label>
                  <div className="mt-1">
                    <input
                      {...register("password", { required: true })}
                      id="password"
                      name="password"
                      type="password"
                      autoComplete="current-password"
                      required
                      className="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                    />
                  </div>
                </div>

                <div>
                  <Button
                    type="submit"
                    classes="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-primary hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary"
                  >
                    Connexion
                  </Button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div className="hidden lg:block relative w-0 flex-1">
        <img
          className="absolute inset-0 h-full w-full object-cover"
          src="/images/signin-register.jpg"
          alt=""
        />
      </div>
    </>
  );
};

export default SignIn;
