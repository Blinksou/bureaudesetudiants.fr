import { FC } from "react";
import Button from "../../components/common/Button";
import { useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import { api } from "../../components/common/api";
import { useRouter } from "next/router";
import Link from "next/link";

type RegisterFormData = {
  email: string;
  password: string;
};

const Register: FC = (): JSX.Element => {
  const router = useRouter();
  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm<RegisterFormData>();

  const onSubmit = async (formData): Promise<void> => {
    try {
      const { data } = await api.post("/auth/register", {
        email: formData.email,
        password: formData.password,
      });
      toast.success("Inscription réussie");
      router.push("/auth/signin");
    } catch (e) {
      toast.error(e.response.data.message);
    }
  };

  return (
    <>
      <div className="flex-1 flex flex-col items-center justify-center py-2 px-4 sm:px-6">
        <div className="mx-auto w-full max-w-sm">
          <div>
            <h2 className="mt-6 text-3xl font-extrabold tracking-wide">
              Je crée mon compte
            </h2>
            <p className="mt-2 text-sm text-gray-600">
              Ou{" "}
              <Link href="/auth/signin">
                <a className="font-medium text-secondary hover:text-sky-700">
                  Je me connecte
                </a>
              </Link>
            </p>
          </div>

          <div className="mt-8">
            <div className="mt-6">
              <form onSubmit={handleSubmit(onSubmit)} className="space-y-6">
                <div>
                  <label
                    htmlFor="email"
                    className="block text-sm font-medium text-gray-700"
                  >
                    Adresse email
                  </label>
                  <div className="mt-1">
                    {errors.email?.type === "required" && "L'email est requis"}
                    <input
                      {...register("email", { required: true })}
                      id="email"
                      name="email"
                      type="email"
                      autoComplete="email"
                      required
                      className="w-full appearance-none px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-primary focus:border-primary sm:text-sm"
                    />
                  </div>
                </div>

                <div className="space-y-1">
                  <label
                    htmlFor="password"
                    className="block text-sm font-medium text-gray-700"
                  >
                    Mot de passe
                  </label>
                  <div className="mt-1">
                    {errors.password && "Le mot de passe est requis"}
                    <input
                      {...register("password", { required: true })}
                      id="password"
                      name="password"
                      type="password"
                      autoComplete="current-password"
                      required
                      className="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                    />
                  </div>
                </div>

                <div className="flex items-center justify-between">
                  <div className="flex items-center">
                    <input
                      id="remember-me"
                      name="remember-me"
                      type="checkbox"
                      className="h-4 w-4 text-indigo-600 focus:ring-indigo-500 border-gray-300 rounded"
                    />
                    <label
                      htmlFor="remember-me"
                      className="ml-2 block text-sm text-gray-900"
                    >
                      Se souvenir de moi
                    </label>
                  </div>

                  <div className="text-sm">
                    <a
                      href="#"
                      className="font-medium text-secondary hover:text-sky-700"
                    >
                      Mot de passe oublié ?
                    </a>
                  </div>
                </div>
                <div>
                  <Button
                    type="submit"
                    classes="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-primary hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary"
                  >
                    Créer mon compte
                  </Button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div className="hidden lg:block relative w-0 flex-1">
        <img
          className="absolute inset-0 h-full w-full object-cover"
          src="/images/signin-register.jpg"
          alt=""
        />
      </div>
    </>
  );
};

export default Register;
