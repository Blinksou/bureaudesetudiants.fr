import { AppProps } from "next/app";
import "../styles/globals.css";
import "../styles/app.css";
import "react-datepicker/dist/react-datepicker.css";
import { ReactElement, ReactNode } from "react";
import Layout from "../components/Layout";
import { useAuth } from "../hooks/useAuth";
import { AuthContext } from "../contexts/authContext";
import EasterEggContainer from "../components/easter-egg/EasterEggContainer";
import { NextComponentType, NextPageContext } from "next/dist/shared/lib/utils";

export type NextPageWithLayout<P = {}, IP = P> = NextComponentType<
  NextPageContext,
  IP,
  P
> & {
  getLayout?: (page: ReactElement) => ReactNode;
};

export type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout;
};

function MyApp({ Component, pageProps }: AppPropsWithLayout) {
  // Auth
  const {
    userId,
    email,
    isAdmin,
    profile,
    token,
    updatedAt,
    login,
    logout,
    isLoggedIn,
  } = useAuth();

  const getLayout = Component.getLayout ?? ((page) => <Layout>{page}</Layout>);

  return (
    <AuthContext.Provider
      value={{
        isLoggedIn,
        userId,
        email,
        isAdmin,
        profile,
        token,
        updatedAt,
        login,
        logout,
      }}
    >
      <EasterEggContainer />
      {getLayout(<Component {...pageProps} />)}
    </AuthContext.Provider>
  );
}

export default MyApp;
