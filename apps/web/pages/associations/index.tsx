import * as React from "react";
import { ReactElement } from "react";
import { NextPageWithLayout } from "../_app";
import AdminAssociationLayout from "./_layout";
import Badge from "../../components/common/Badge";
import {
  formatAssociationStatus,
  getAssociationStatusBadgeType,
} from "../../lib/associations";
import { Association } from "prisma-types";
import { GetServerSideProps } from "next";
import { api } from "../../components/common/api";
import Link from "next/link";

export interface AssociationWithAuthorization extends Association {
  isAllowed: true;
}

interface PageProps {
  associations?: Array<AssociationWithAuthorization>;
}

const Admin: NextPageWithLayout<PageProps> = ({ associations = [] }) => {
  return (
    <main className="flex-1">
      <div className="py-6">
        <div className="px-4 sm:px-6 md:px-0">
          <h1 className="text-2xl font-semibold text-gray-900">
            Mes associations
          </h1>
        </div>
        <div className="px-4 sm:px-6 md:px-0 py-10">
          <ul
            role="list"
            className="grid grid-cols-1 gap-6 sm:grid-cols-2 lg:grid-cols-3"
          >
            {associations.map((association) => (
              <li
                key={association.id}
                className="col-span-1 bg-white rounded-lg shadow divide-y divide-gray-200 shadow-md border"
              >
                <div className="w-full flex items-center justify-between p-6 space-x-6">
                  <div className="flex-1 truncate">
                    <div className="flex items-center space-x-3">
                      <h3 className="text-gray-900 text-sm font-medium truncate">
                        {association.name}
                      </h3>
                      <Badge
                        label={formatAssociationStatus(association.status)}
                        type={getAssociationStatusBadgeType(association.status)}
                      />
                    </div>
                    <p className="mt-1 text-gray-500 text-sm truncate">
                      {association.description}
                    </p>
                  </div>
                </div>
                <div>
                  <div className="-mt-px flex divide-x divide-gray-200">
                    <div className="w-0 flex-1 flex">
                      <Link href={`/associations/${association.id}`}>
                        <a className="relative -mr-px w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-gray-700 font-medium border border-transparent rounded-bl-lg hover:bg-gray-100">
                          <span className="ml-3">Voir</span>
                        </a>
                      </Link>
                    </div>
                    {association.isAllowed && (
                      <div className="-ml-px w-0 flex-1 flex">
                        <Link href={`/associations/${association.id}/edit`}>
                          <a
                            data-testid={`editBtn${association.id}`}
                            className="relative w-0 flex-1 inline-flex items-center justify-center py-4 text-sm text-gray-700 font-medium border border-transparent rounded-br-lg hover:bg-gray-100"
                          >
                            <span className="ml-3">Editer</span>
                          </a>
                        </Link>
                      </div>
                    )}
                  </div>
                </div>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </main>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  let { data: associations } = await api.get(`/user/me/associations`, {
    headers: {
      Authorization: "Bearer " + context.req.cookies["user.token"],
    },
  });

  associations = await Promise.all(
    associations.map(async (asso) => {
      try {
        const { data: isAllowed } = await api.post(
          `/associations/${asso.id}/users/isAllowed`,
          {
            associationId: asso.id,
            roles: ["PRESIDENT"],
          },
          {
            headers: {
              "Content-Type": "application/json",
              Authorization: "Bearer " + context.req.cookies["user.token"],
            },
          }
        );

        asso.isAllowed = isAllowed;
      } catch (e) {
        asso.isAllowed = false;
      }

      return asso;
    })
  );

  return {
    props: {
      associations,
    },
  };
};

Admin.getLayout = function getLayout(page: ReactElement) {
  return <AdminAssociationLayout>{page}</AdminAssociationLayout>;
};

export default Admin;
