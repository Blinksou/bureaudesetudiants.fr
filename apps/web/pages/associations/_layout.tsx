import * as React from "react";
import { HomeIcon, PlusCircleIcon } from "@heroicons/react/outline";
import BaseAdminLayout from "../../components/admin/BaseAdminLayout";

interface Props {
  children: React.ReactChildren | React.ReactElement;
}

const BASE_PATH = "/associations";

const navigation = [
  {
    name: "Mes associations",
    href: BASE_PATH,
    icon: HomeIcon,
  },
  {
    name: "Rejoindre une association",
    href: "/join-an-association",
    icon: PlusCircleIcon,
  },
];

const AdminAssociationLayout: React.FC<Props> = ({ children }) => {
  return <BaseAdminLayout navigation={navigation}>{children}</BaseAdminLayout>;
};

export default AdminAssociationLayout;
