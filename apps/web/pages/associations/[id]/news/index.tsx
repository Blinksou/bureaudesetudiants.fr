import { GetServerSideProps } from "next";
import { api } from "../../../../components/common/api";
import * as React from "react";
import { ReactElement, useState } from "react";
import AdminAssociationLayout from "../../_layout";
import { News } from "prisma-types";
import { NextPageWithLayout } from "../../../_app";
import { useForm } from "react-hook-form";
import Button from "../../../../components/common/Button";
import Modal from "../../../../components/common/Modal";
import { toast } from "react-hot-toast";
import { EyeIcon, NewspaperIcon, PencilIcon } from "@heroicons/react/outline";
import Link from "next/link";

type PageProps = {
  news: News[];
  associationId: string;
  isAllowed: boolean;
};

const News: NextPageWithLayout<PageProps> = ({
  news,
  associationId,
  isAllowed,
}) => {
  const [refNews, setRefNews] = useState<Array<News>>(news);

  const [isNewsModalOpen, setIsNewsModalOpen] = useState<boolean>(false);

  const [editedNews, setEditedNews] = useState<News>(null);

  const {
    register,
    formState: { errors },
    handleSubmit,
    setValue,
  } = useForm<{
    title: string;
    content: string;
  }>();

  const openNewsModal = (news) => {
    setIsNewsModalOpen(true);

    if (news) {
      setEditedNews(news);

      setValue("title", news.title);
      setValue("content", news.content);
    }
  };

  const closeNewsModal = () => {
    setIsNewsModalOpen(false);

    setTimeout(() => {
      setEditedNews(null);

      setValue("title", null);
      setValue("content", null);
    }, 150);
  };

  const onSubmit = async (formData): Promise<void> => {
    try {
      const { data } = await api.post(
        `/associations/${associationId}/news/`,
        formData
      );

      setRefNews([...refNews, data]);

      toast.success("Actualité créée");
    } catch (e) {
      toast.error(e.response.data.message);
    } finally {
      setIsNewsModalOpen(false);
    }
  };

  const onEditSubmit = async (formData): Promise<void> => {
    try {
      const { data } = await api.patch(`/news/${editedNews.id}/`, formData);

      const index = refNews.findIndex((news) => news.id === data.id);
      news[index] = data;

      setRefNews([...news]);

      toast.success("Actualité modifiée");
    } catch (e) {
      toast.error(e.response.data.message);
    } finally {
      setIsNewsModalOpen(false);
    }
  };

  return (
    <>
      {isAllowed && (
        <div className="flex justify-end py-6">
          <button
            onClick={() => openNewsModal(null)}
            className="px-4 py-2 bg-primary text-white font-semibold rounded-md hover:bg-blue-700"
          >
            Ajouter une actualité
          </button>
        </div>
      )}

      {refNews.length > 0 ? (
        <div className="mt-8 flex flex-col">
          <div className="-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div className="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
              <div className="overflow-hidden shadow ring-1 ring-black ring-opacity-5 md:rounded-lg">
                <table className="min-w-full divide-y divide-gray-300">
                  <thead className="bg-gray-50">
                    <tr>
                      <th
                        scope="col"
                        className="py-3.5 pl-4 pr-3 text-left text-xs tracking-wider text-gray-600 font-light uppercase sm:pl-6"
                      >
                        Titre
                      </th>
                      <th
                        scope="col"
                        className="px-3 py-3.5 text-left text-xs tracking-wider text-gray-600 font-light uppercase"
                      >
                        Créée le
                      </th>
                      <th
                        scope="col"
                        className="px-3 py-3.5 text-left text-xs tracking-wider text-gray-600 font-light uppercase"
                      >
                        Modifiée le
                      </th>
                      <th
                        scope="col"
                        className="relative py-3.5 pl-3 pr-4 sm:pr-6"
                      >
                        <span className="sr-only">Edit</span>
                      </th>
                    </tr>
                  </thead>
                  <tbody className="divide-y divide-gray-200 bg-white">
                    {refNews.map((n) => (
                      <tr key={n.id}>
                        <td className="whitespace-nowrap py-4 pl-4 pr-3 text-sm sm:pl-6">
                          <div className="flex items-center">
                            <div className="h-10 w-10 flex-shrink-0 text-gray-300">
                              <NewspaperIcon />
                            </div>
                            <div className="ml-4">
                              <div className="font-semibold">{n.title}</div>
                            </div>
                          </div>
                        </td>
                        <td className="whitespace-nowrap px-3 py-4 text-sm">
                          <span className="font-light">
                            {new Date(n.createdAt).toLocaleDateString()}
                          </span>
                        </td>
                        <td className="whitespace-nowrap px-3 py-4 text-sm">
                          <span className="font-light">
                            {new Date(n.updatedAt).toLocaleDateString()}
                          </span>
                        </td>
                        <td className="relative whitespace-nowrap py-4 pl-3 pr-4 text-right text-sm font-medium sm:pr-6">
                          {isAllowed && (
                            <button
                              onClick={() => openNewsModal(n)}
                              className="text-primary hover:text-indigo-900 flex"
                            >
                              <PencilIcon className="h-5 w-5 mr-2" />
                              Editer
                            </button>
                          )}
                          <Link
                            href={`/associations/${n.associationId}/news/${n.id}`}
                          >
                            <a className="text-primary hover:text-indigo-900 flex">
                              <EyeIcon className="h-5 w-5 mr-2" />
                              Voir
                            </a>
                          </Link>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div className="flex justify-center py-12 text-sm font-light">
          {"Vous n'avez pas encore d'actualités."}
        </div>
      )}

      <Modal
        isOpen={isNewsModalOpen}
        onClose={closeNewsModal}
        title={`${editedNews ? "Modifier" : "Ajouter"} une actualité`}
      >
        <form
          onSubmit={handleSubmit(editedNews ? onEditSubmit : onSubmit)}
          className="flex flex-col gap-y-4"
        >
          <div className={""}>
            {errors.title && <span>Ce champs est requis</span>}
            <label
              htmlFor={"title"}
              className="block text-sm font-medium text-gray-700"
            >
              Titre
            </label>
            <input
              {...register("title", { required: true })}
              type="text"
              autoComplete={"off"}
              required
              className="w-full appearance-none px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-primary focus:border-primary sm:text-sm"
            />
          </div>

          <div className={""}>
            {errors.content && <span>Ce champs est requis</span>}
            <label
              htmlFor={"content"}
              className="block text-sm font-medium text-gray-700"
            >
              Contenu
            </label>
            <textarea
              {...register("content", { required: true })}
              autoComplete={"off"}
              required
              className="w-full appearance-none px-3 py-2 border h-20 border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-primary focus:border-primary sm:text-sm"
            />
          </div>

          <div>
            <Button
              type="submit"
              classes="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-primary hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary"
            >
              Valider
            </Button>
          </div>
        </form>
      </Modal>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { id } = context.query;

  const { data } = await api.get(`/associations/${id}/news`, {
    headers: {
      Authorization: "Bearer " + context.req.cookies["user.token"],
    },
  });

  const { data: isAllowed } = await api.post(
    `/associations/${id}/users/isAllowed`,
    {
      associationId: id,
      roles: [
        "PRESIDENT",
        "COMMUNITY_MANAGER",
        "EVENT_MANAGER",
        "SCHOOL_ADMIN",
      ],
    },
    {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + context.req.cookies["user.token"],
      },
    }
  );

  return {
    props: {
      associationId: id,
      news: data,
      isAllowed,
    },
  };
};

News.getLayout = function getLayout(page: ReactElement) {
  return <AdminAssociationLayout>{page}</AdminAssociationLayout>;
};

export default News;
