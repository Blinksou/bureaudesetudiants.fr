import { NextPageWithLayout } from "../../../_app";
import { News } from "prisma-types";
import * as React from "react";
import { ReactElement } from "react";
import { GetServerSideProps } from "next";
import { api } from "../../../../components/common/api";
import AdminAssociationLayout from "../../_layout";
import { CalendarIcon, ChevronLeftIcon } from "@heroicons/react/outline";
import Link from "next/link";

interface PageProps {
  news: News;
}

const ShowNews: NextPageWithLayout<PageProps> = ({ news }) => {
  return (
    <main className="flex-1">
      <div className="space-y-4 md:space-y-6">
        <div className="border-b border-gray-200 pb-2 px-4">
          <Link href={`/associations/${news.associationId}/news`}>
            <a
              className={"flex items-center text-gray-500 hover:text-gray-700"}
            >
              <ChevronLeftIcon className={"h-4"} /> Retour
            </a>
          </Link>
        </div>
        <div className="sm:px-6 md:px-0 pb-2 px-4">
          <div className="text-gray-500 flex items-center">
            <CalendarIcon className={"mr-1 h-5 w-5"} aria-hidden="true" />
            {new Date(news.createdAt).toLocaleDateString()}
          </div>
          <h1 className="text-2xl font-bold text-primary">{news.title}</h1>
        </div>
        <div className="sm:px-6 md:px-0 pb-2 px-4">{news.content}</div>
      </div>
    </main>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { newsId } = context.query;

  const { data: news } = await api.get(`/news/${newsId}`, {
    headers: {
      Authorization: "Bearer " + context.req.cookies["user.token"],
    },
  });

  return {
    props: {
      news,
    },
  };
};

ShowNews.getLayout = function getLayout(page: ReactElement) {
  return <AdminAssociationLayout>{page}</AdminAssociationLayout>;
};

export default ShowNews;
