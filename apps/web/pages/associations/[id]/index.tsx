import * as React from "react";
import { Fragment, ReactElement, useContext, useState } from "react";
import AdminAssociationLayout from "../_layout";
import { api } from "../../../components/common/api";
import { NextPageWithLayout } from "../../_app";
import { Association, AssociationsUsers, Profile, User } from "prisma-types";
import { GetServerSideProps } from "next";
import Link from "next/link";
import {
  ASSOCIATIONS_ROLES,
  getAssociationRoleFromAssociationUser,
} from "../../../lib/roles";
import Modal from "../../../components/common/Modal";
import Button from "../../../components/common/Button";
import { useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import { Menu, Transition } from "@headlessui/react";
import { DotsHorizontalIcon } from "@heroicons/react/outline";
import { AuthContext } from "../../../contexts/authContext";

export type AssociationUserWithProfile = AssociationsUsers & {
  user: User & {
    profile: Profile;
  };
};

interface PageProps {
  association: Association;
  associationUser: AssociationsUsers;
  members: Array<AssociationUserWithProfile>;
}
const ShowAssociation: NextPageWithLayout<PageProps> = ({
  association,
  associationUser,
  members,
}) => {
  const [editModalIsOpen, setEditModalIsOpen] = useState(false);
  const [memberIndex, setMemberIndex] = useState<number>();
  const [editedMember, setEditedMember] =
    useState<AssociationUserWithProfile>(null);

  const [reactiveMembers, setReactiveMembers] =
    useState<Array<AssociationUserWithProfile>>(members);

  const {
    register,
    formState: { errors },
    handleSubmit,
    setValue,
  } = useForm<{
    role: string;
  }>();
  const openEditModal = (member: AssociationUserWithProfile, i: number) => {
    setEditedMember(member);
    setMemberIndex(i);

    setValue("role", member.role);

    setEditModalIsOpen(true);
  };

  const closeEditModal = () => {
    setEditModalIsOpen(false);

    setTimeout(() => {
      setEditedMember(null);
    }, 150);
  };

  const onSubmit = async (formData): Promise<void> => {
    const { role } = formData;
    try {
      const { data } = await api.put(
        `/associations/${association.id}/users/${editedMember.userId}`,
        {
          role,
        }
      );

      reactiveMembers[memberIndex].role = role;

      setReactiveMembers([...reactiveMembers]);

      toast.success("Membre modifié avec succès");
      setEditModalIsOpen(false);
    } catch (e) {
      toast.error(e.response.data.message);
    }
  };

  type NavigationItem = {
    name: string;
    type: "link" | "button;";
    href?: string;
    onClick?: () => void;
  };

  const actionNavigation: Array<NavigationItem> = [
    { name: "Voir les actualités", type: "link", href: "news" },
    { name: "Voir les évènements", type: "link", href: "events" },
  ];

  const user = useContext(AuthContext);

  return (
    <>
      <div className="min-h-full">
        <main className="py-10">
          {/* Page header */}
          <div className="max-w-3xl mx-auto px-4 sm:px-6 md:flex md:items-center md:justify-between md:space-x-5 lg:max-w-7xl lg:px-8">
            <div className="flex items-center space-x-5">
              <div>
                <h1 className="text-2xl font-bold text-gray-900">
                  {association.name}
                </h1>
                <p className="text-sm font-medium text-gray-500">
                  {association.description}
                </p>
              </div>
            </div>
            <div className="mt-6 flex flex-col-reverse justify-stretch space-y-4 space-y-reverse sm:flex-row-reverse sm:justify-end sm:space-x-reverse sm:space-y-0 sm:space-x-3 md:mt-0 md:flex-row md:space-x-3">
              {(associationUser.role === "PRESIDENT" || user.isAdmin) && (
                <Link href={`/associations/${association.id}/edit`}>
                  <a
                    data-testid={"editBtn"}
                    className="inline-flex items-center justify-center px-4 py-2 border border-gray-300 shadow-sm text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-blue-500"
                  >
                    Editer
                  </a>
                </Link>
              )}
              <Menu as="div" className="ml-3 relative">
                <div>
                  <Menu.Button className="inline-flex items-center justify-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-blue-500">
                    <DotsHorizontalIcon height={"20px"} />
                  </Menu.Button>
                </div>
                <Transition
                  as={Fragment}
                  enter="transition ease-out duration-100"
                  enterFrom="transform opacity-0 scale-95"
                  enterTo="transform opacity-100 scale-100"
                  leave="transition ease-in duration-75"
                  leaveFrom="transform opacity-100 scale-100"
                  leaveTo="transform opacity-0 scale-95"
                >
                  <Menu.Items className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 py-1 focus:outline-none">
                    {actionNavigation.map((item) => (
                      <Menu.Item key={item.name}>
                        {({ active }) => {
                          if (item.type === "link") {
                            return (
                              <Link
                                href={`/associations/${association.id}/${item.href}`}
                              >
                                <a
                                  className={`
                                      ${active ? "bg-gray-100" : ""} 
                                      block py-2 px-4 text-sm text-gray-700
                                    `}
                                >
                                  {item.name}
                                </a>
                              </Link>
                            );
                          } else {
                            return (
                              <button
                                onClick={item?.onClick}
                                className={`
                                      ${active ? "bg-gray-100" : ""} 
                                      w-full text-left py-2 px-4 text-sm text-gray-700
                                    `}
                              >
                                {item.name}
                              </button>
                            );
                          }
                        }}
                      </Menu.Item>
                    ))}
                  </Menu.Items>
                </Transition>
              </Menu>
            </div>
          </div>

          <div className="mt-8 max-w-3xl mx-auto gap-6 sm:px-6 lg:max-w-7xl">
            <div className="space-y-6">
              {/* Description list*/}
              <section aria-labelledby="applicant-information-title">
                <div className="bg-white shadow sm:rounded-lg">
                  <div className="px-4 py-5 sm:px-6">
                    <h2
                      id="applicant-information-title"
                      className="text-lg leading-6 font-medium text-gray-900"
                    >
                      Informations générales
                    </h2>
                    <p className="mt-1 max-w-2xl text-sm text-gray-500">
                      Créée le{" "}
                      {new Date(association.createdAt).toLocaleDateString("fr")}
                    </p>
                  </div>
                  <div className="border-t border-gray-200 px-4 py-5 sm:px-6">
                    <dl className="grid grid-cols-1 gap-x-4 gap-y-8 sm:grid-cols-2">
                      <div className="sm:col-span-1">
                        <dt className="text-sm font-medium text-gray-500">
                          Ville
                        </dt>
                        <dd className="mt-1 text-sm text-gray-900">
                          {association.city}
                        </dd>
                      </div>
                      <div className="sm:col-span-1">
                        <dt className="text-sm font-medium text-gray-500">
                          Pays
                        </dt>
                        <dd className="mt-1 text-sm text-gray-900">
                          {association.country}
                        </dd>
                      </div>
                      <div className="sm:col-span-1">
                        <dt className="text-sm font-medium text-gray-500">
                          Adresse
                        </dt>
                        <dd className="mt-1 text-sm text-gray-900">
                          {association.address1}
                        </dd>
                      </div>
                      <div className="sm:col-span-1">
                        <dt className="text-sm font-medium text-gray-500">
                          {"Complément d'adresse"}
                        </dt>
                        <dd className="mt-1 text-sm text-gray-900">
                          {association.address2}
                        </dd>
                      </div>
                      <div className="sm:col-span-2">
                        <dt className="text-sm font-medium text-gray-500">
                          Code postal
                        </dt>
                        <dd className="mt-1 text-sm text-gray-900">
                          {association.zipCode}
                        </dd>
                      </div>
                    </dl>
                  </div>
                  <div className={"bg-gray-50 px-4 py-6 sm:px-6"}></div>
                </div>
              </section>

              {/* Members */}
              <section aria-labelledby="notes-title">
                <div className="bg-white shadow sm:rounded-lg sm:overflow-hidden">
                  <div className="divide-y divide-gray-200">
                    <div className="px-4 py-5 sm:px-6">
                      <h2
                        id="notes-title"
                        className="text-lg font-medium text-gray-900"
                      >
                        Membres
                      </h2>
                    </div>
                    <div className="px-4 py-6 sm:px-6">
                      <ul role="list" className="space-y-8">
                        {members.map((member, i) => (
                          <li key={member.userId}>
                            <div className="flex space-x-3">
                              <div>
                                <div className="text-sm">
                                  <a
                                    href="#"
                                    className="font-medium text-gray-900"
                                  >
                                    {member.user.profile ? (
                                      <>
                                        {member.user.profile.firstname}{" "}
                                        {member.user.profile.lastname}
                                      </>
                                    ) : (
                                      <>{member.user.email}</>
                                    )}
                                  </a>
                                </div>
                                <div className="mt-1 text-sm text-gray-700">
                                  <p>
                                    {getAssociationRoleFromAssociationUser(
                                      member
                                    )}
                                  </p>
                                </div>
                                <div className="mt-2 text-sm space-x-2">
                                  <span className="text-gray-500 font-medium">
                                    Rejoint le{" "}
                                    {new Date(
                                      member.createdAt
                                    ).toLocaleDateString("fr")}
                                  </span>{" "}
                                  <span className="text-gray-500 font-medium">
                                    &middot;
                                  </span>{" "}
                                  {((associationUser.userId !== member.userId &&
                                    associationUser.role === "PRESIDENT") ||
                                    user.isAdmin) && (
                                    <button
                                      data-testid={`editMemberBtn${member.userId}`}
                                      type="button"
                                      onClick={() => openEditModal(member, i)}
                                      className="text-gray-900 font-medium"
                                    >
                                      Editer
                                    </button>
                                  )}
                                </div>
                              </div>
                            </div>
                          </li>
                        ))}
                      </ul>
                    </div>
                  </div>
                  <div className="bg-gray-50 px-4 py-6 sm:px-6" />
                </div>
              </section>
            </div>
          </div>
        </main>
      </div>

      <Modal
        isOpen={editModalIsOpen}
        onClose={closeEditModal}
        title={`Editer ${editedMember ? editedMember.user.email : ""}`}
      >
        <form onSubmit={handleSubmit(onSubmit)} className="space-y-6">
          <div className={"mt-1"}>
            <label
              htmlFor={"role"}
              className="block text-sm font-medium text-gray-700"
            >
              Rôle
            </label>
            <select
              {...register("role", { required: true })}
              id={`role`}
              name={`role`}
              required={true}
              data-dropdown-toggle="dropdown"
              className="w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-primary focus:border-primary sm:text-sm"
            >
              {ASSOCIATIONS_ROLES.map((role, i) => {
                return (
                  <option key={i} value={role.value}>
                    {role.name}
                  </option>
                );
              })}
            </select>
          </div>

          <div>
            <Button
              type="submit"
              classes="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-primary hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary"
            >
              Valider
            </Button>
          </div>
        </form>
      </Modal>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { id } = context.query;

  const { data } = await api.get(`/associations/${id}`, {
    headers: {
      Authorization: "Bearer " + context.req.cookies["user.token"],
    },
  });

  const { data: associationUser } = await api.get(
    `/associations/${id}/users/me/get`,
    {
      headers: {
        Authorization: "Bearer " + context.req.cookies["user.token"],
      },
    }
  );

  const { data: members } = await api.get(`/associations/${id}/users`, {
    headers: {
      Authorization: "Bearer " + context.req.cookies["user.token"],
    },
  });

  return {
    props: {
      association: data.data,
      associationUser,
      members,
    },
  };
};

ShowAssociation.getLayout = function getLayout(page: ReactElement) {
  return <AdminAssociationLayout>{page}</AdminAssociationLayout>;
};

export default ShowAssociation;
