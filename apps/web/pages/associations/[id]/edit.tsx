import * as React from "react";
import { ReactElement } from "react";
import AdminAssociationLayout from "../_layout";
import { api } from "../../../components/common/api";
import { NextPageWithLayout } from "../../_app";
import { Association } from "prisma-types";
import { GetServerSideProps } from "next";
import { useForm } from "react-hook-form";
import { AssociationRegisterFormData, fields } from "../register";
import { toast } from "react-hot-toast";
import Button from "../../../components/common/Button";
import { useRouter } from "next/router";

interface PageProps {
  association: Association;
}

const EditAssociation: NextPageWithLayout<PageProps> = ({ association }) => {
  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm<AssociationRegisterFormData>();

  const router = useRouter();

  const onSubmit = async (formData): Promise<void> => {
    const { name, description, address1, address2, city, zipCode, country } =
      formData;
    try {
      const { data } = await api.put(`/associations/${association.id}`, {
        name,
        description,
        address1,
        address2,
        city,
        zipCode,
        country,
      });
      toast.success("Association modifiée avec succès");

      await router.push(`/associations/${association.id}`);
    } catch (e) {
      toast.error(e.response.data.message);
    }
  };

  return (
    <main className="flex-1">
      <div className="py-6">
        <div className="px-4 sm:px-6 md:px-0">
          <h1 className="text-2xl font-semibold text-gray-900">
            Edition de {association.name}
          </h1>
        </div>
        <div className="px-4 sm:px-6 md:px-0">
          <form onSubmit={handleSubmit(onSubmit)} className="space-y-6">
            {fields.map((field, index) => {
              const { name, toString, required, errors } = field;
              return (
                <div key={index}>
                  <label
                    htmlFor={name}
                    className="block text-sm font-medium text-gray-700"
                  >
                    {toString}
                  </label>
                  <div className="mt-1">
                    {errors[name]?.type === "required" && `${errors.required}`}
                    <input
                      {...register(name, { required: required })}
                      id={`${name}`}
                      name={`${name}`}
                      type="text"
                      autoComplete={`${name}`}
                      required={required}
                      defaultValue={association[name]}
                      className="w-full appearance-none px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-primary focus:border-primary sm:text-sm"
                    />
                  </div>
                </div>
              );
            })}

            <div>
              <Button
                type="submit"
                classes="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-primary hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary"
              >
                Valider
              </Button>
            </div>
          </form>
        </div>
      </div>
    </main>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { id } = context.query;

  const { data: isAllowed } = await api.post(
    `/associations/${id}/users/isAllowed`,
    {
      associationId: id,
      roles: ["PRESIDENT"],
    },
    {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + context.req.cookies["user.token"],
      },
    }
  );

  if (!isAllowed) {
    return {
      redirect: {
        destination: `/associations/${id}`,
        permanent: false,
      },
    };
  }

  const { data: association } = await api.get(`/associations/${id}`, {
    headers: {
      Authorization: "Bearer " + context.req.cookies["user.token"],
    },
  });

  return {
    props: {
      association: association.data,
    },
  };
};

EditAssociation.getLayout = function getLayout(page: ReactElement) {
  return <AdminAssociationLayout>{page}</AdminAssociationLayout>;
};

export default EditAssociation;
