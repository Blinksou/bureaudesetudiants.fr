import * as React from "react";
import { ReactElement } from "react";
import AdminAssociationLayout from "../_layout";
import { api } from "../../../components/common/api";
import { NextPageWithLayout } from "../../_app";
import { Association } from "prisma-types";
import { GetServerSideProps } from "next";
import { useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import Button from "../../../components/common/Button";
import { ASSOCIATIONS_ROLES } from "../../../lib/roles";

interface PageProps {
  association: Association;
}

const AddAssociationMember: NextPageWithLayout<PageProps> = ({
  association,
}) => {
  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm<{ email: string; role: string }>();

  const onSubmit = async (formData): Promise<void> => {
    const { email, role } = formData;
    try {
      const { data } = await api.post(`/associations/${association.id}/users`, {
        email,
        role,
      });
      toast.success("Membre rajouté avec succès");
    } catch (e) {
      toast.error(e.response.data.message);
    }
  };

  return (
    <main className="flex-1">
      <div className="py-6">
        <div className="px-4 sm:px-6 md:px-0">
          <h1 className="text-2xl font-semibold text-gray-900">
            Ajouter un membre dans {association.name}
          </h1>
        </div>
        <div className="px-4 sm:px-6 md:px-0">
          <form onSubmit={handleSubmit(onSubmit)} className="space-y-6">
            <div className="mt-10">
              <label
                htmlFor={"email"}
                className="block text-sm font-medium text-gray-700"
              >
                Email du membre
              </label>
              <input
                {...register("email", { required: true })}
                id={`email`}
                name={`email`}
                type="email"
                autoComplete={`email`}
                required={true}
                className="w-full appearance-none px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-primary focus:border-primary sm:text-sm"
              />
            </div>

            <div className={"mt-1"}>
              <label
                htmlFor={"role"}
                className="block text-sm font-medium text-gray-700"
              >
                Rôle
              </label>
              <select
                {...register("role", { required: true })}
                id={`role`}
                name={`role`}
                required={true}
                className="w-full appearance-none px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-primary focus:border-primary sm:text-sm"
              >
                {ASSOCIATIONS_ROLES.map((role, i) => {
                  return (
                    <option key={i} value={role.value}>
                      {role.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div>
              <Button
                type="submit"
                classes="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-primary hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary"
              >
                Valider
              </Button>
            </div>
          </form>
        </div>
      </div>
    </main>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { id } = context.query;

  const { data: isAllowed } = await api.post(
    `/associations/${id}/users/isAllowed`,
    {
      associationId: id,
      roles: ["PRESIDENT"],
    },
    {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + context.req.cookies["user.token"],
      },
    }
  );

  if (!isAllowed) {
    return {
      redirect: {
        destination: `/associations/${id}`,
        permanent: false,
      },
    };
  }

  const { data: association } = await api.get(`/associations/${id}`, {
    headers: {
      Authorization: "Bearer " + context.req.cookies["user.token"],
    },
  });

  return {
    props: {
      association: association.data,
    },
  };
};

AddAssociationMember.getLayout = function getLayout(page: ReactElement) {
  return <AdminAssociationLayout>{page}</AdminAssociationLayout>;
};

export default AddAssociationMember;
