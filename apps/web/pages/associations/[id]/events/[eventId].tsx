import { NextPageWithLayout } from "../../../_app";
import { AssociationsUsers, Event } from "prisma-types";
import * as React from "react";
import { ReactElement, useState } from "react";
import { GetServerSideProps } from "next";
import { api } from "../../../../components/common/api";
import AdminAssociationLayout from "../../_layout";
import { CalendarIcon, ChevronLeftIcon } from "@heroicons/react/outline";
import Link from "next/link";
import { toast } from "react-hot-toast";
import { ParticipantCard } from "../../../../components/events/participantCard";
import { UserWithProfile } from "api/lib/user/types";

type EventWithSubscribedUsers = Event & {
  users: Array<AssociationsUsers & { user: UserWithProfile }>;
};

interface PageProps {
  event: EventWithSubscribedUsers;
  isSubscribed: boolean;
}

const ShowEvent: NextPageWithLayout<PageProps> = ({ event, isSubscribed }) => {
  const [isUserSubscribed, setIsUserSubscribed] =
    useState<boolean>(isSubscribed);

  const registerEvent = async () => {
    try {
      const test = await api.get(
        `/events/${event.id}/${isUserSubscribed ? "unsubscribe" : "subscribe"}`
      );
      setIsUserSubscribed(isUserSubscribed ? false : true);
      toast.success(
        isUserSubscribed
          ? "Vous êtes désinscrit à cet évènement !"
          : "Vous êtes inscrit à cet évènement !"
      );
    } catch (e) {
      toast.error(e.message);
    }
  };

  return (
    <main className="flex-1">
      <div className="space-y-4 md:space-y-6">
        <div className="border-b border-gray-200 pb-2 px-4">
          <Link href={`/associations/${event.associationId}/events`}>
            <a
              className={"flex items-center text-gray-500 hover:text-gray-700"}
            >
              <ChevronLeftIcon className={"h-4"} /> Retour
            </a>
          </Link>
        </div>
        <div className="sm:px-6 md:px-0 pb-2 px-4">
          <div className="text-gray-500 flex items-center">
            <CalendarIcon className={"mr-1 h-5 w-5"} aria-hidden="true" />
            {new Date(event.createdAt).toLocaleDateString()}
          </div>
          <h1 className="text-2xl font-bold text-primary">{event.name}</h1>
          <button
            onClick={registerEvent}
            className={
              "px-4 py-2 font-semibold text-sm text-white bg-primary rounded-md hover:bg-blue-700"
            }
          >
            {isUserSubscribed ? "Se désinscrire" : "S'inscrire"}
          </button>
        </div>
        {event.price && <div className={"py-6"}>Prix {event.price}</div>}
        <div className="sm:px-6 md:px-0 pb-2 px-4">
          <h2 className="font-bold text-xl">Détails</h2>
          {event.description}
        </div>
        <div className="sm:px-6 md:px-0 pb-2 px-4">
          <h2 className="font-bold text-xl">
            Participants (
            {`${event.users.length} participant${
              event.users.length > 1 ? "s" : ""
            }`}
            )
          </h2>
          <div className="flex flex-wrap py-1">
            {event.users.map(({ user }) => {
              return (
                <div>
                  <ParticipantCard
                    name={`${user.profile.firstname} ${user.profile.lastname}`}
                  />
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </main>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { eventId } = context.query;

  const { data: event } = await api.get(`/events/${eventId}`, {
    headers: {
      Authorization: "Bearer " + context.req.cookies["user.token"],
    },
  });

  const { data: isSubscribed } = await api.get(
    `/events/${eventId}/isSubscribed`,
    {
      headers: {
        Authorization: "Bearer " + context.req.cookies["user.token"],
      },
    }
  );

  return {
    props: {
      event,
      isSubscribed,
    },
  };
};

ShowEvent.getLayout = function getLayout(page: ReactElement) {
  return <AdminAssociationLayout>{page}</AdminAssociationLayout>;
};

export default ShowEvent;
