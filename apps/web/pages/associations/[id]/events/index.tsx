import * as React from "react";
import { ReactElement, useState } from "react";
import AdminAssociationLayout from "../../_layout";
import { api } from "../../../../components/common/api";
import { NextPageWithLayout } from "../../../_app";
import { Association, Event } from "prisma-types";
import { GetServerSideProps } from "next";
import { formatDistanceToNowStrict } from "date-fns";
import { ClipboardListIcon } from "@heroicons/react/outline";
import Link from "next/link";
import { useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import Modal from "../../../../components/common/Modal";
import Button from "../../../../components/common/Button";
import DatePicker, { registerLocale } from "react-datepicker";
import fr from "date-fns/locale/fr";

registerLocale("fr", fr);

interface PageProps {
  association: Association;
  events: Array<Event>;
  isAllowed: boolean;
}

const ShowEvents: NextPageWithLayout<PageProps> = ({
  association,
  events,
  isAllowed,
}) => {
  const [refEvents, setRefEvents] = useState<Array<Event>>(events);

  const [isCreateModalOpen, setIsCreateModalOpen] = useState<boolean>(false);

  const [startDate, setStartDate] = useState<Date>(new Date());
  const [endDate, setEndDate] = useState<Date>();

  const handleStartDateChange = (date: Date) => {
    setValue("startDate", date);
    setStartDate(date);
  };

  const handleEndDateChange = (date: Date) => {
    setValue("endDate", date);
    setEndDate(date);
  };

  const {
    register,
    formState: { errors },
    handleSubmit,
    setValue,
  } = useForm<{
    name: string;
    description: string;
    price: number;
    isPublic: boolean;
    startDate: Date;
    endDate: Date;
  }>();

  const openCreateModal = () => {
    setIsCreateModalOpen(true);
  };

  const closeCreateModal = () => {
    setIsCreateModalOpen(false);
  };

  const onSubmit = async (formData): Promise<void> => {
    try {
      formData.isPublic === 1
        ? (formData.isPublic = true)
        : (formData.isPublic = false);

      formData.startDate = new Date(startDate).toISOString();

      if (endDate) {
        formData.endDate = new Date(endDate).toISOString();
      }

      if (!formData.price) {
        delete formData.price;
      }

      const { data } = await api.post(
        `/associations/${association.id}/events/`,
        formData
      );

      setRefEvents([...refEvents, data]);

      toast.success("Evènement créé");
    } catch (e) {
      toast.error(e.response.data.message);
    } finally {
      setIsCreateModalOpen(false);
    }
  };
  return (
    <main className="flex-1">
      <div className="py-6">
        <div className="px-4 sm:px-6 md:px-0 border-b border-gray-200 pb-2 flex justify-between">
          <h1 className="text-lg font-bold text-primary">
            Evènements de {association.name}
          </h1>
          <div>
            {isAllowed && (
              <button
                onClick={() => {
                  openCreateModal();
                }}
                className="px-4 py-2 bg-primary text-white font-semibold rounded-md hover:bg-blue-700"
              >
                Ajouter un évènement
              </button>
            )}
          </div>
        </div>
        <div className="px-4 sm:px-6 md:px-0">
          <ul role="list" className="divide-y divide-gray-200">
            {refEvents.map((event) => (
              <li
                key={event.id}
                className="py-4 flex justify-between items-center"
              >
                <div className="ml-3">
                  <div className={"flex gap-x-4"}>
                    <ClipboardListIcon className={"h-16 text-gray-300 "} />
                    <div className={"flex flex-col"}>
                      <p className="text-sm font-medium text-gray-900">
                        {event.name}
                      </p>
                      {event.price && (
                        <p className="text-sm text-gray-500">{event.price}</p>
                      )}
                      <p className="text-sm text-gray-500">
                        Evènement {event.isPublic ? "public" : "privé"}
                      </p>
                      <p className="text-sm text-gray-500">
                        Le {new Date(event.startDate).toLocaleDateString()}
                      </p>
                    </div>
                  </div>
                </div>
                <div>
                  {new Date(event.startDate) > new Date() ? (
                    <span className={"text-primary font-bold text-2xl"}>
                      Dans{" "}
                      {formatDistanceToNowStrict(new Date(event.startDate), {
                        unit: "day",
                        locale: fr,
                      })}
                    </span>
                  ) : (
                    <span className={"text-gray-500"}>Evènement passé</span>
                  )}

                  <div className={"mt-2"}>
                    <Link
                      href={`/associations/${association.id}/events/${event.id}`}
                    >
                      <a
                        className={
                          "px-4 py-2 font-semibold text-sm text-white bg-primary rounded-md hover:bg-blue-700"
                        }
                      >
                        Voir
                      </a>
                    </Link>
                  </div>
                </div>
              </li>
            ))}
          </ul>
        </div>
      </div>

      <Modal
        isOpen={isCreateModalOpen}
        onClose={closeCreateModal}
        title={`Ajouter un évènement à ${association.name}`}
      >
        <form
          onSubmit={handleSubmit(onSubmit)}
          className="flex flex-col gap-y-4"
        >
          <div className={""}>
            {errors.name && <span>Ce champs est requis</span>}
            <label
              htmlFor={"title"}
              className="block text-sm font-medium text-gray-700"
            >
              Titre
            </label>
            <input
              {...register("name", { required: true })}
              type="text"
              autoComplete={"off"}
              required
              className="w-full appearance-none px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-primary focus:border-primary sm:text-sm"
            />
          </div>

          <div className={""}>
            {errors.description && <span>Ce champs est requis</span>}
            <label
              htmlFor={"content"}
              className="block text-sm font-medium text-gray-700"
            >
              Contenu
            </label>
            <textarea
              {...register("description", { required: true })}
              autoComplete={"off"}
              required
              className="w-full appearance-none px-3 py-2 border h-20 border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-primary focus:border-primary sm:text-sm"
            />
          </div>

          <div className={""}>
            {errors.price && <span>Ce champ doit être un nombre valide</span>}
            <label
              htmlFor={"price"}
              className="block text-sm font-medium text-gray-700"
            >
              Prix
            </label>
            <input
              type={"number"}
              {...register("price")}
              autoComplete={"off"}
              className="w-full appearance-none px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-primary focus:border-primary sm:text-sm"
            />
          </div>

          <div className={""}>
            <input
              id={"isPublic"}
              type={"checkbox"}
              {...register("isPublic")}
              autoComplete={"off"}
              className="form-check-input appearance-none h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
            />
            <label
              htmlFor={"isPublic"}
              className="block text-sm font-medium text-gray-700"
            >
              Public
            </label>
          </div>

          <div className={""}>
            {errors.startDate && <span>Ce champ est requis</span>}
            <label
              htmlFor={"isPublic"}
              className="block text-sm font-medium text-gray-700"
            >
              Date de début
            </label>
            <DatePicker
              selected={startDate}
              onChange={(date) => handleStartDateChange(date)}
              className="w-full appearance-none px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-primary focus:border-primary sm:text-sm"
              locale="fr"
            />
          </div>

          <div className={""}>
            <label
              htmlFor={"isPublic"}
              className="block text-sm font-medium text-gray-700"
            >
              Date de fin
            </label>
            <DatePicker
              selected={endDate}
              onChange={(date) => handleEndDateChange(date)}
              className="w-full appearance-none px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-primary focus:border-primary sm:text-sm"
              locale="fr"
            />
          </div>

          <div>
            <Button
              type="submit"
              classes="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-primary hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary"
            >
              Valider
            </Button>
          </div>
        </form>
      </Modal>
    </main>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { id } = context.query;

  const { data: events } = await api.get(`/associations/${id}/events`, {
    headers: {
      Authorization: "Bearer " + context.req.cookies["user.token"],
    },
  });

  const { data: association } = await api.get(`/associations/${id}`, {
    headers: {
      Authorization: "Bearer " + context.req.cookies["user.token"],
    },
  });

  const { data: isAllowed } = await api.post(
    `/associations/${association.data.id}/users/isAllowed`,
    {
      associationId: association.data.id,
      roles: ["PRESIDENT"],
    },
    {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + context.req.cookies["user.token"],
      },
    }
  );

  return {
    props: {
      association: association.data,
      events,
      isAllowed,
    },
  };
};

ShowEvents.getLayout = function getLayout(page: ReactElement) {
  return <AdminAssociationLayout>{page}</AdminAssociationLayout>;
};

export default ShowEvents;
