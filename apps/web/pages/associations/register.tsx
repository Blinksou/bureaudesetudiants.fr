import { FC, useContext, useEffect } from "react";
import Button from "../../components/common/Button";
import { AcademicCapIcon } from "@heroicons/react/outline";
import { useForm } from "react-hook-form";
import { api } from "../../components/common/api";
import { toast } from "react-hot-toast";
import { AuthContext } from "../../contexts/authContext";
import { useRouter } from "next/router";

export type AssociationRegisterFormData = {
  name: string;
  description: string;
  address1: string;
  address2?: string;
  city: string;
  zipCode: string;
  country: string;
};

export const fields: {
  name:
    | "name"
    | "description"
    | "address1"
    | "address2"
    | "city"
    | "zipCode"
    | "country";
  toString: string;
  required: boolean;
  errors: { required: string };
}[] = [
  {
    name: "name",
    toString: "Nom",
    required: true,
    errors: { required: "Le nom est requis" },
  },
  {
    name: "description",
    toString: "Description",
    required: true,
    errors: { required: "La description est requise" },
  },
  {
    name: "address1",
    toString: "Adresse",
    required: true,
    errors: { required: "L'adrese est requise" },
  },
  {
    name: "address2",
    toString: "Complément d'adresse",
    required: false,
    errors: { required: "" },
  },
  {
    name: "city",
    toString: "Ville",
    required: true,
    errors: { required: "La ville est requise" },
  },
  {
    name: "zipCode",
    toString: "Code postal",
    required: true,
    errors: { required: "Le code postal est requis" },
  },
  {
    name: "country",
    toString: "Pays",
    required: true,
    errors: { required: "Le pays est requis" },
  },
];

const Register: FC = (): JSX.Element => {
  const user = useContext(AuthContext);
  const router = useRouter();

  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm<AssociationRegisterFormData>();

  useEffect(() => {
    if (!user.isLoggedIn) {
      toast.error("Vous devez être connecté pour ajouter un bde");
      router.push("/auth/signin");
    }
  }, []);

  const onSubmit = async (formData): Promise<void> => {
    const { name, description, address1, address2, city, zipCode, country } =
      formData;
    try {
      const { data } = await api.post("/associations", {
        name,
        description,
        address1,
        address2,
        city,
        zipCode,
        country,
      });
      toast.success(
        "L'inscription de votre BDE a été soumise pour révision. Nous vous recontacterons prochainement."
      );
      router.push("/associations");
    } catch (e) {
      toast.error(e.response.data.message);
    }
  };
  return (
    <>
      <div className="flex-1 flex flex-col items-center justify-center py-2 px-4 sm:px-6">
        <div className="mx-auto w-full max-w-sm">
          <div>
            <div className="flex justify-center">
              <AcademicCapIcon className="h-16 w-16 text-primary" />
            </div>
            <h2 className="mt-6 text-3xl font-extrabold tracking-wide">
              Je créer mon BDE
            </h2>
          </div>

          <div className="mt-8">
            <div className="mt-6">
              <form onSubmit={handleSubmit(onSubmit)} className="space-y-6">
                {fields.map((field, index) => {
                  const { name, toString, required, errors } = field;
                  return (
                    <div key={index}>
                      <label
                        htmlFor={name}
                        className="block text-sm font-medium text-gray-700"
                      >
                        {toString}
                      </label>
                      <div className="mt-1">
                        {errors[name]?.type === "required" &&
                          `${errors.required}`}
                        <input
                          {...register(name, { required: required })}
                          id={`${name}`}
                          name={`${name}`}
                          type="text"
                          autoComplete={`${name}`}
                          required={required}
                          className="w-full appearance-none px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-primary focus:border-primary sm:text-sm"
                        />
                      </div>
                    </div>
                  );
                })}

                <div>
                  <Button
                    type="submit"
                    classes="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-primary hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary"
                  >
                    Créer mon BDE
                  </Button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div className="hidden lg:block relative w-0 flex-1">
        <div className="absolute inset-0 h-full w-full object-cover bg-secondary" />
      </div>
    </>
  );
};

export default Register;
