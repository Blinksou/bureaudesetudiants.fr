import * as React from "react";
import { ReactElement, useState } from "react";
import { GetServerSideProps } from "next";
import { Association } from "prisma-types";
import axios from "axios";
import { toast } from "react-hot-toast";
import Button from "../../components/common/Button";
import AdminPlatformLayout from "./_layout";
import Modal from "../../components/common/Modal";
import { useForm } from "react-hook-form";
import { AssociationRegisterFormData, fields } from "../associations/register";
import { api } from "../../components/common/api";
import {
  formatAssociationStatus,
  getAssociationStatusBadgeType,
} from "../../lib/associations";
import Badge from "../../components/common/Badge";
import { NextPageWithLayout } from "../_app";
import Link from "next/link";

interface PageProps {
  associations: Array<Association>;
}

const PlatformAssociations: NextPageWithLayout<PageProps> = ({
  associations,
}) => {
  const [assos, setAssos] = useState(associations);

  const approve = async (association: Association, i: number) => {
    const response = await axios.put(
      `${process.env.NEXT_PUBLIC_API_URL}/associations/${association.id}/status`,
      {
        status: "VALIDATE",
      }
    );

    if (response.status !== 200) {
      toast.error(response.data.error ?? "Une erreur  est survenue.");
      return;
    }

    associations[i].status = "VALIDATE";
    setAssos([...associations]);
  };

  const [editModalIsOpen, setEditModalIsOpen] = useState(false);
  const [editedAssociation, setEditedAssociation] = useState<Association>(null);

  const {
    register,
    formState: { errors },
    handleSubmit,
    setValue,
  } = useForm<AssociationRegisterFormData>();

  const openEditModal = (association: Association) => {
    setEditedAssociation(association);

    setValue("name", association.name);
    setValue("address1", association.address1);
    setValue("country", association.country);
    setValue("zipCode", association.zipCode);
    setValue("city", association.city);
    setValue("address2", association.address2);
    setValue("description", association.description);

    setEditModalIsOpen(true);
  };

  const closeEditModal = () => {
    setEditModalIsOpen(false);

    setTimeout(() => {
      setEditedAssociation(null);
    }, 150);
  };

  const onSubmit = async (formData): Promise<void> => {
    const { name, description, address1, address2, city, zipCode, country } =
      formData;
    try {
      const { data } = await api.put(`/associations/${editedAssociation.id}`, {
        name,
        description,
        address1,
        address2,
        city,
        zipCode,
        country,
      });
      toast.success("Association modifiée avec succès");
    } catch (e) {
      toast.error(e.response.data.message);
    }
  };

  return (
    <>
      <div className="px-4 sm:px-6 lg:px-8">
        <div className="sm:flex sm:items-center">
          <div className="sm:flex-auto">
            <h1 className="text-xl font-semibold text-gray-900">
              Associations
            </h1>
            <p className="mt-2 text-sm text-gray-700">
              Liste des associations inscrites sur la plateforme
            </p>
          </div>
        </div>
        <div className="mt-8 flex flex-col">
          <div className="-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div className="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
              <div className="overflow-hidden shadow ring-1 ring-black ring-opacity-5 md:rounded-lg">
                <table className="min-w-full divide-y divide-gray-300">
                  <thead className="bg-gray-50">
                    <tr>
                      <th
                        scope="col"
                        className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6"
                      >
                        Dénomination
                      </th>
                      <th
                        scope="col"
                        className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900"
                      >
                        Localisation
                      </th>
                      <th
                        scope="col"
                        className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900"
                      >
                        Statut
                      </th>
                      <th
                        scope="col"
                        className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900"
                      >
                        Date de création
                      </th>
                      <th
                        scope="col"
                        className="relative py-3.5 pl-3 pr-4 sm:pr-6"
                      >
                        <span className="sr-only">Voir</span>
                      </th>
                    </tr>
                  </thead>
                  <tbody className="divide-y divide-gray-200 bg-white">
                    {assos.map((association, i) => (
                      <tr key={association.id}>
                        <td className="whitespace-nowrap py-4 pl-4 pr-3 text-sm sm:pl-6">
                          <div className="flex items-center">
                            <div className="ml-4">
                              <div className="font-medium text-gray-900">
                                {association.name}
                              </div>
                            </div>
                          </div>
                        </td>
                        <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                          <div className="text-gray-900">
                            {association.zipCode}
                          </div>
                          <div className="text-gray-500">
                            {association.country}
                          </div>
                        </td>
                        <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                          <Badge
                            label={formatAssociationStatus(association.status)}
                            type={getAssociationStatusBadgeType(
                              association.status
                            )}
                          />
                        </td>
                        <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                          {new Date(association.createdAt).toLocaleDateString()}
                        </td>
                        <td className="relative whitespace-nowrap py-4 px-2 flex gap-x-2 justify-end items-center text-sm font-medium">
                          <Link href={`/associations/${association.id}`}>
                            <a className="bg-primary text-white font-semibold px-4 py-2 cursor-pointer rounded-md hover:bg-blue-700">
                              Voir
                            </a>
                          </Link>

                          {association.status === "WAITING_APPROVAL" && (
                            <button
                              data-testid={`approveBtn${association.id}`}
                              type={"button"}
                              className="bg-green-600 text-white rounded-md px-4 py-2 font-semibold hover:bg-green-700"
                              onClick={() => {
                                approve(association, i);
                              }}
                            >
                              Approuver
                            </button>
                          )}

                          <button
                            type="button"
                            data-testid={`editBtn${association.id}`}
                            onClick={() => openEditModal(association)}
                            className="px-4 py-2 font-semibold text-white bg-gray-400 rounded-md hover:bg-gray-500"
                          >
                            Editer
                          </button>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Modal
        isOpen={editModalIsOpen}
        onClose={closeEditModal}
        title={`Editer ${
          editedAssociation ? editedAssociation.name.toUpperCase() : ""
        }`}
      >
        <form onSubmit={handleSubmit(onSubmit)} className="space-y-6">
          {fields.map((field, index) => {
            const { name, toString, required, errors } = field;
            return (
              <div key={index}>
                <label
                  htmlFor={name}
                  className="block text-sm font-medium text-gray-700"
                >
                  {toString}
                </label>
                <div className="mt-1">
                  {errors[name]?.type === "required" && `${errors.required}`}
                  <input
                    {...register(name, { required: required })}
                    id={`${name}`}
                    name={`${name}`}
                    type="text"
                    autoComplete={`${name}`}
                    required={required}
                    className="w-full appearance-none px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-primary focus:border-primary sm:text-sm"
                  />
                </div>
              </div>
            );
          })}

          <div>
            <Button
              type="submit"
              classes="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-primary hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary"
            >
              Valider
            </Button>
          </div>
        </form>
      </Modal>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const response = await api.get(`/associations`, {
    headers: {
      Authorization: "Bearer " + context.req.cookies["user.token"],
    },
  });

  return {
    props: {
      associations: response.data,
    },
  };
};

PlatformAssociations.getLayout = function getLayout(page: ReactElement) {
  return <AdminPlatformLayout>{page}</AdminPlatformLayout>;
};

export default PlatformAssociations;
