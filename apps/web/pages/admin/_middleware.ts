import type { NextFetchEvent, NextRequest } from "next/server";
import { NextResponse } from "next/server";

export async function middleware(req: NextRequest, ev: NextFetchEvent) {
  const token = req.cookies["user.token"];

  if (!token) {
    return NextResponse.redirect("/auth/signin");
  }

  const response = await fetch(
    `${process.env.NEXT_PUBLIC_API_URL}/user/access/platform`,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );

  const data = await response.json();

  if (!data.isAdmin) {
    return NextResponse.redirect("/");
  }

  return NextResponse.next();
}
