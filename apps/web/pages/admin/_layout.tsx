import * as React from "react";
import { HomeIcon, OfficeBuildingIcon } from "@heroicons/react/outline";
import BaseAdminLayout from "../../components/admin/BaseAdminLayout";

interface Props {
  children: React.ReactChildren | React.ReactElement;
}

const BASE_PATH = "/admin";

const navigation = [
  {
    name: "Tableau de bord",
    href: BASE_PATH,
    icon: HomeIcon,
  },
  {
    name: "Gestion des associations",
    href: `${BASE_PATH}/associations`,
    icon: OfficeBuildingIcon,
  },
];

const AdminPlatformLayout: React.FC<Props> = ({ children }) => {
  return <BaseAdminLayout navigation={navigation}>{children}</BaseAdminLayout>;
};

export default AdminPlatformLayout;
