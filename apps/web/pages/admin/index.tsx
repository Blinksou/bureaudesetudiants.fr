import * as React from "react";
import { ReactElement } from "react";
import { NextPageWithLayout } from "../_app";
import AdminPlatformLayout from "./_layout";

const PlatformAdmin: NextPageWithLayout = () => {
  return <></>;
};

PlatformAdmin.getLayout = function getLayout(page: ReactElement) {
  return <AdminPlatformLayout>{page}</AdminPlatformLayout>;
};

export default PlatformAdmin;
