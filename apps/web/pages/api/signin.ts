import { NextApiRequest, NextApiResponse } from "next";
import Cookies from "cookies";
import { api } from "../../components/common/api";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { email, password } = req.body as { email: string; password: string };

  const { data } = await api.post("/auth/login", {
    email: email,
    password: password,
  });

  const cookies = new Cookies(req, res);

  cookies.set("user.token", data.token, {
    httpOnly: true,
  });

  res.status(200).json({ data });
}
