import { Context, createContext } from "react";
import { IUserDto } from "../hooks/useAuth";

export interface IAuthContext {
  isLoggedIn: boolean;
  userId: string;
  email: string;
  isAdmin: boolean;
  profile?: string;
  token: string;
  updatedAt: Date;
  login: (data: IUserDto) => void;
  logout: () => void;
}

export const AuthContext: Context<IAuthContext> = createContext({
  isLoggedIn: false,
  userId: "",
  email: "",
  isAdmin: false,
  profile: null,
  token: "",
  updatedAt: new Date(1900, 0, 1),
  login: () => {},
  logout: () => {},
});
