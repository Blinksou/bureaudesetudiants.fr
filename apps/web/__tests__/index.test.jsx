import {render, screen} from '@testing-library/react'
import Home from '../pages/index'

describe('Home',  () => {
    it('renders landing page', async () => {

        const roles = ['copyrights','trusting-brand', 'testimony', 'footer', 'feature', 'carousel'];

        render(<Home />)

        for (let role of roles){
            expect(screen.getByRole(role)).toBeInTheDocument()
        }
    })
})