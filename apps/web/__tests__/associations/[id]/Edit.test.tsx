import { render, screen } from "@testing-library/react";
import EditAssociation from "../../../pages/associations/[id]/edit";
import { AssociationWithAuthorization } from "../../../pages/associations";

const association: AssociationWithAuthorization = {
  id: "1",
  name: "Association 1",
  description: "Description 1",
  isAllowed: true,
  address1: "Address 1",
  address2: "Address 2",
  city: "City",
  country: "France",
  zipCode: "33000",
  status: "VALIDATE",
  createdAt: new Date(),
  updatedAt: new Date(),
};

describe("Associations", () => {
  it("renders associations page with association informations", async () => {
    render(<EditAssociation association={association} />);

    expect(
      screen.getByText(association.name, { exact: false })
    ).toBeInTheDocument();
  });
});
