import { render, screen } from "@testing-library/react";
import ShowAssociation, {
  AssociationUserWithProfile,
} from "../../../pages/associations/[id]";
import { AssociationWithAuthorization } from "../../../pages/associations";

const association: AssociationWithAuthorization = {
  id: "1",
  name: "Association 1",
  description: "Description 1",
  isAllowed: true,
  address1: "Address 1",
  address2: "Address 2",
  city: "City",
  country: "France",
  zipCode: "33000",
  status: "VALIDATE",
  createdAt: new Date(),
  updatedAt: new Date(),
};

const associationMember: AssociationUserWithProfile = {
  associationId: "1",
  userId: "1",
  role: "PRESIDENT",
  createdAt: new Date(),
  updatedAt: new Date(),
  user: {
    id: "1",
    isAdmin: true,
    email: "test@demo.fr",
    password: "password",
    createdAt: new Date(),
    updatedAt: new Date(),
    profile: {
      id: "1",
      userId: "1",
      createdAt: new Date(),
      updatedAt: new Date(),
      avatar: null,
      firstname: "John",
      lastname: "Doe",
      phone: "+33123456789",
    },
  },
};

const associationUserNotAllowed = { ...associationMember };
associationUserNotAllowed.userId = "2";
associationUserNotAllowed.user.id = "2";
associationUserNotAllowed.user.profile.id = "2";
associationUserNotAllowed.user.profile.userId = "2";
associationUserNotAllowed.role = "MEMBER";

const members: AssociationUserWithProfile[] = [
  associationMember,
  associationUserNotAllowed,
];

describe("Show Association", () => {
  it("renders association page with details", async () => {
    render(
      <ShowAssociation
        association={association}
        members={members}
        associationUser={associationMember}
      />
    );

    expect(screen.getByText(association.name)).toBeInTheDocument();
    expect(screen.getByText(association.city)).toBeInTheDocument();
    expect(screen.getByText(association.country)).toBeInTheDocument();
    expect(screen.getByText(association.zipCode)).toBeInTheDocument();
  });

  it("renders edit button if isAllowed", async () => {
    render(
      <ShowAssociation
        association={association}
        members={members}
        associationUser={associationMember}
      />
    );

    expect(screen.getByTestId(`editBtn`)).toBeInTheDocument();
  });

  it("does not render edit button if not isAllowed", async () => {
    render(
      <ShowAssociation
        association={association}
        members={members}
        associationUser={associationUserNotAllowed}
      />
    );

    expect(screen.queryByTestId(`editBtn`)).not.toBeInTheDocument();
  });

  it("renders edit MEMBER button if isAllowed", async () => {
    render(
      <ShowAssociation
        association={association}
        members={members}
        associationUser={associationMember}
      />
    );

    for (let member of members) {
      if (member.userId === associationMember.userId) {
        expect(
          screen.queryByTestId(`editMemberBtn${member.userId}`)
        ).not.toBeInTheDocument();
      } else {
        expect(
          screen.getByTestId(`editMemberBtn${member.userId}`)
        ).toBeInTheDocument();
      }
    }
  });
});
