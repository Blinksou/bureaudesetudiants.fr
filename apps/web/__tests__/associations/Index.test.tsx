import { render, screen } from "@testing-library/react";
import Admin, { AssociationWithAuthorization } from "../../pages/associations";

const associations: Array<AssociationWithAuthorization> = [
  {
    id: "1",
    name: "Association 1",
    description: "Description 1",
    isAllowed: true,
    address1: "Address 1",
    address2: "Address 2",
    city: "City",
    country: "France",
    zipCode: "33000",
    status: "VALIDATE",
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    id: "2",
    name: "Association 2",
    description: "Description 2",
    isAllowed: true,
    address1: "Address 1",
    address2: "Address 2",
    city: "City",
    country: "France",
    zipCode: "33000",
    status: "WAITING_APPROVAL",
    createdAt: new Date(),
    updatedAt: new Date(),
  },
];

describe("Associations", () => {
  it("renders associations page with associations", async () => {
    render(<Admin associations={associations} />);

    for (let asso of associations) {
      expect(screen.getByText(asso.name)).toBeInTheDocument();
    }
  });

  it("renders edit button if isAllowed", async () => {
    render(<Admin associations={associations} />);

    for (let asso of associations) {
      expect(screen.getByTestId(`editBtn${asso.id}`)).toBeInTheDocument();
    }
  });
});
