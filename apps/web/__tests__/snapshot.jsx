import {render} from '@testing-library/react'
import Home from '../pages/index'
import {Header} from "../components/Header";
import UserMenu from "../components/user-menu/UserMenu";
import SignIn from "../pages/auth/signin";
import Register from "../pages/auth/register";

import BaseAdminLayout from "../components/admin/BaseAdminLayout";
import Admin from "../pages/associations";
import ShowAssociation from "../pages/associations/[id]";
import AddAssociationMember from "../pages/associations/[id]/add";
import EditAssociation from "../pages/associations/[id]/edit";
import ShowEvents from "../pages/associations/[id]/events";
import ShowNews from "../pages/associations/[id]/news/[newsId]";
import News from "../pages/associations/[id]/news";
import Layout from "../components/Layout";
import EasterEgg from "../components/easter-egg/EasterEgg";
import EasterEggContainer from "../components/easter-egg/EasterEggContainer";
import Badge from "../components/common/Badge";
import Modal from "../components/common/Modal";
import Button from "../components/common/Button";

it('renders homepage unchanged', () => {
    const { container } = render(<Home />)
    expect(container).toMatchSnapshot()
})

it('renders header unchanged', () => {
    const { container } = render(<Header />)
    expect(container).toMatchSnapshot()
})

it('renders user menu unchanged', () => {
    const { container } = render(<UserMenu />)
    expect(container).toMatchSnapshot()
})

it('renders signin page unchanged', () => {
    const { container } = render(<SignIn />)
    expect(container).toMatchSnapshot()
})

it('renders register page unchanged', () => {
    const { container } = render(<Register />)
    expect(container).toMatchSnapshot()
})

it('renders rBaseAdminLayout', () => {
    const { container } = render(<BaseAdminLayout />)
    expect(container).toMatchSnapshot()
})

it('renders associations index page unchanged', () => {
    const { container } = render(<Admin />)
    expect(container).toMatchSnapshot()
})

const date = new Date();
const association = {
    id: 'ass1',
    name: 'name',
    description: '',
    address1: '',
    address2: '' | null,
    city: '',
    zipCode: '',
    country: '',
    status: "VALIDATE",
    createdAt: date,
    updatedAt: date,
};
const associationUser = {
    role: "MEMBER",
    userId: '',
    associationId: '',
    createdAt: date,
    updatedAt: date,
}

it('renders association page unchanged', () => {
    const {container} = render(<ShowAssociation association={association} associationUser={associationUser} members={[]} />)
    expect(container).toMatchSnapshot()
})

it('renders associations add page unchanged', () => {
    const { container } = render(<AddAssociationMember association={association}/>)
    expect(container).toMatchSnapshot()
})

it('renders associations edit page unchanged', () => {
    const { container } = render(<EditAssociation association={association}/>)
    expect(container).toMatchSnapshot()
})

it('renders associations events page unchanged', () => {
    const { container } = render(<ShowEvents association={association} events={[]} isAllowed={true}/>)
    expect(container).toMatchSnapshot()
})

it('renders associations index news page unchanged', () => {
    const { container } = render(<News news={[]} associationId={'1'} isAllowed={false}/>)
    expect(container).toMatchSnapshot()
})
const news = {
    id:'',
    title:'',
    content:'',
    userId:'',
    associationId:'1',
    createdAt:date,
    updatedAt:date
}
it('renders associations one news page unchanged', () => {
    const { container } = render(<ShowNews news={news}/>)
    expect(container).toMatchSnapshot()
})

it('renders layout unchanged', () => {
    const { container } = render(<Layout/>)
    expect(container).toMatchSnapshot()
})

it('renders easter egg unchanged', () => {
    const { container } = render(<EasterEgg open={false} end={() => {}} />)
    expect(container).toMatchSnapshot()
})

it('renders easter egg container unchanged', () => {
    const { container } = render(<EasterEggContainer />)
    expect(container).toMatchSnapshot()
})

it('renders badge unchanged', () => {
    const { container } = render(<Badge label={''} />)
    expect(container).toMatchSnapshot()
})

it('renders Modal unchanged', () => {
    const { container } = render(<Modal title={''} isOpen={false}/>)
    expect(container).toMatchSnapshot()
})

it('renders button unchanged', () => {
    const { container } = render(<Button />)
    expect(container).toMatchSnapshot()
})


