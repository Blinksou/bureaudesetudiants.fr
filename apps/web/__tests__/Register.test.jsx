import {render, screen} from '@testing-library/react'
import Register from '../pages/auth/register';

describe('Signin',  () => {
    it('renders signin', async () => {

        const inputs = ['Adresse email', 'Mot de passe'];

        render(<Register />)

        for (let input of inputs){
            expect(screen.getByLabelText(input)).toBeInTheDocument()
        }

        const button = screen.getByRole('button', { name: 'Créer mon compte' });

        expect(button).toBeInTheDocument();
        expect(button).not.toBeDisabled();
    })
})