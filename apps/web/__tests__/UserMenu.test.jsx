import {fireEvent, render, screen, waitFor} from '@testing-library/react'
import UserMenu from "../components/user-menu/UserMenu";

describe('UserMenu',  () => {
    it('renders user menu', async () => {

        const roles = ['opens-user-menu'];

        render(<UserMenu />)

        const button = screen.getByRole(roles[0]);

        fireEvent.click(button);

        const authLink = screen.getByRole('menuitem', {
            name: /Se connecter/i
        })

        await waitFor(() => authLink)

        expect(authLink).toHaveTextContent(/Se connecter/i);
    })
})