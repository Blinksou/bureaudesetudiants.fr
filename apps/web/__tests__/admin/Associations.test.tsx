import { render, screen } from "@testing-library/react";
import { Association } from "prisma-types";
import PlatformAssociations from "../../pages/admin/associations";

const associations: Array<Association> = [
  {
    id: "1",
    name: "Association 1",
    description: "Description 1",
    address1: "Address 1",
    address2: "Address 2",
    city: "City",
    country: "France",
    zipCode: "33000",
    status: "VALIDATE",
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    id: "2",
    name: "Association 2",
    description: "Description 2",
    address1: "Address 1",
    address2: "Address 2",
    city: "City",
    country: "France",
    zipCode: "33000",
    status: "WAITING_APPROVAL",
    createdAt: new Date(),
    updatedAt: new Date(),
  },
];

describe("Associations", () => {
  it("renders associations page with associations", async () => {
    render(<PlatformAssociations associations={associations} />);

    for (let asso of associations) {
      expect(screen.getByText(asso.name)).toBeInTheDocument();
    }
  });

  it("renders edit button for each associations", async () => {
    render(<PlatformAssociations associations={associations} />);

    for (let asso of associations) {
      expect(screen.getByTestId(`editBtn${asso.id}`)).toBeInTheDocument();
    }
  });

  it("renders approbation button for each associations which wait for approval", async () => {
    render(<PlatformAssociations associations={associations} />);

    for (let asso of associations) {
      if (asso.status === "WAITING_APPROVAL") {
        expect(screen.getByTestId(`approveBtn${asso.id}`)).toBeInTheDocument();
      }
    }
  });

  it("does not render approbation button for each associations which does not wait for approval", async () => {
    render(<PlatformAssociations associations={associations} />);

    for (let asso of associations) {
      if (asso.status !== "WAITING_APPROVAL") {
        expect(
          screen.queryByTestId(`approveBtn${asso.id}`)
        ).not.toBeInTheDocument();
      }
    }
  });
});
