import {render, screen} from '@testing-library/react'
import SignIn from "../pages/auth/signin";

describe('Signin',  () => {
    it('renders signin', async () => {

        const inputs = ['Adresse email', 'Mot de passe'];

        render(<SignIn />)

        for (let input of inputs){
            expect(screen.getByLabelText(input)).toBeInTheDocument()
        }

        const button = screen.getByRole('button', { name: 'Connexion' });

        expect(button).toBeInTheDocument();
        expect(button).not.toBeDisabled();
    })
})