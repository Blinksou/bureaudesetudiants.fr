import {render, screen} from '@testing-library/react'
import {Header} from "../components/Header";

describe('Header',  () => {
    it('renders header', async () => {

        const links = ['Nos solutions', 'Associations', 'Actualités', 'Contact'];

        render(<Header />)

        for (let link of links){
            expect(screen.getByRole('link', {
                name: new RegExp(link, 'i')
            })).toBeInTheDocument()
        }
    })
})