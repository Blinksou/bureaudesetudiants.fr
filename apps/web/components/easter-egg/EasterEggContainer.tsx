import { useState } from "react";
import EasterEgg from "./EasterEgg";

function EasterEggContainer(): JSX.Element {
  const [shouldTriggerEgg, setShouldTriggerEgg] = useState<boolean>(false);

  const onMouseEnter = (event): void => {
    if (event.ctrlKey) {
      setShouldTriggerEgg(true);
    }
  };

  const endEasterEgg = (): void => {
    setShouldTriggerEgg(false);
  };

  return (
    <>
      <EasterEgg open={shouldTriggerEgg} end={endEasterEgg} />
      <div
        onMouseEnter={(event) => {
          onMouseEnter(event);
        }}
        className="fixed top-0 right-0 h-12 w-12"
      />
    </>
  );
}

export default EasterEggContainer;
