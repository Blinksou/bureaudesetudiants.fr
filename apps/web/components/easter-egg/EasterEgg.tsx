import ReactConfetti from "react-confetti";
import { useWindowSize } from "react-use";

type EasterEggProps = {
  open: boolean;
  end: () => void;
};

function EasterEgg({ open, end }: EasterEggProps) {
  const { width, height } = useWindowSize();

  if (!open) {
    return null;
  }

  return (
    <>
      <ReactConfetti
        onConfettiComplete={end}
        recycle={false}
        confettiSource={{
          w: 10,
          h: 10,
          x: width / 2,
          y: height / 2,
        }}
      />
    </>
  );
}

export default EasterEgg;
