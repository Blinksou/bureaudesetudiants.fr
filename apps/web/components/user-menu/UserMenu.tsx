import { Menu, Transition } from "@headlessui/react";
import { Fragment, useContext } from "react";
import Link from "next/link";
import { MenuIcon } from "@heroicons/react/outline";
import { UserCircleIcon } from "@heroicons/react/solid";
import { useRouter } from "next/router";
import { AuthContext } from "../../contexts/authContext";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

function CustomLink(props) {
  let { href, children, ...rest } = props;
  return (
    <Link href={href}>
      <a {...rest}>{children}</a>
    </Link>
  );
}

function UserMenu(): JSX.Element {
  const router = useRouter();
  const auth = useContext(AuthContext);

  const handleLogout = () => {
    auth.logout();
    router.push("/auth/signin");
  };

  return (
    <>
      <Menu as="div" className="relative inline-block text-left z-50 w-52">
        <div>
          <Menu.Button
            role={"opens-user-menu"}
            className="rounded-full flex items-center ml-auto md:m-auto xl:ml-auto text-gray-400 hover:text-gray-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-primary"
          >
            <span className="sr-only">Open menu</span>
            <MenuIcon className="h-9 w-9 block lg:hidden" aria-hidden="true" />
            <UserCircleIcon
              className="h-9 w-9 hidden lg:block"
              aria-hidden="true"
            />
          </Menu.Button>
        </div>

        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
            <div className="py-1">
              {auth.isAdmin && (
                <Menu.Item>
                  {({ active }) => (
                    <CustomLink
                      href="/admin/associations"
                      className={classNames(
                        active ? "bg-gray-100 text-gray-900" : "text-gray-700",
                        "block px-4 py-2 text-sm"
                      )}
                    >
                      Admin
                    </CustomLink>
                  )}
                </Menu.Item>
              )}
              {auth.isLoggedIn ? (
                <>
                  <Menu.Item>
                    {({ active }) => (
                      <CustomLink
                        href="/associations"
                        className={classNames(
                          active
                            ? "bg-gray-100 text-gray-900"
                            : "text-gray-700",
                          "block px-4 py-2 text-sm"
                        )}
                      >
                        Tableau de bord
                      </CustomLink>
                    )}
                  </Menu.Item>
                  <Menu.Item>
                    {({ active }) => (
                      <CustomLink
                        href="/association/register"
                        className={classNames(
                          active
                            ? "bg-gray-100 text-gray-900"
                            : "text-gray-700",
                          "block px-4 py-2 text-sm"
                        )}
                      >
                        Inscrire mon asso
                      </CustomLink>
                    )}
                  </Menu.Item>
                  <Menu.Item>
                    {({ active }) => (
                      <button
                        type="submit"
                        className={classNames(
                          active
                            ? "bg-gray-100 text-gray-900"
                            : "text-gray-700",
                          "block w-full text-left px-4 py-2 text-sm"
                        )}
                        onClick={handleLogout}
                      >
                        Se déconnecter
                      </button>
                    )}
                  </Menu.Item>
                </>
              ) : (
                <>
                  <Menu.Item>
                    {({ active }) => (
                      <CustomLink
                        href="/auth/signin"
                        className={classNames(
                          active
                            ? "bg-gray-100 text-gray-900"
                            : "text-gray-700",
                          "block px-4 py-2 text-sm"
                        )}
                      >
                        Se connecter
                      </CustomLink>
                    )}
                  </Menu.Item>
                  <Menu.Item>
                    {({ active }) => (
                      <CustomLink
                        href="/auth/register"
                        className={classNames(
                          active
                            ? "bg-gray-100 text-gray-900"
                            : "text-gray-700",
                          "block px-4 py-2 text-sm"
                        )}
                      >
                        {"S'inscrire"}
                      </CustomLink>
                    )}
                  </Menu.Item>
                </>
              )}
            </div>
          </Menu.Items>
        </Transition>
      </Menu>
    </>
  );
}

export default UserMenu;
