import { Header } from "./Header";
import { Toaster } from "react-hot-toast";

export default function Layout({ children }): JSX.Element {
  return (
    <>
      <div className="min-h-screen flex flex-col">
        <Toaster position={"bottom-center"} />
        <Header />
        <main className="flex flex-grow justify-center">{children}</main>
      </div>
    </>
  );
}
