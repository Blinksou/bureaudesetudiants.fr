import Link from "next/link";
import { FC } from "react";
import { AcademicCapIcon } from "@heroicons/react/outline";
import UserMenu from "./user-menu/UserMenu";

export const Header: FC = (): JSX.Element => {
  return (
    <header className="px-6 py-6 flex justify-between items-center sticky top-0 inset-x-0 z-50 bg-white border shadow-md">
      <Link href="/">
        <a className="flex gap-x-2 items-center text-primary w-52">
          <AcademicCapIcon className="h-10 w-10" />
          <span className="text-2xl font-light font-pacifico">Mon Bde</span>
        </a>
      </Link>
      <nav className="flex text-sm w-full items-center justify-end">
        <div className="hidden lg:flex justify-between w-full px-16 lg:px-20 xl:px-32 font-semibold text-lg text-primary items-center">
          <a href={"#"} className="py-1 px-4 rounded-md hover:bg-blue-100">
            Nos solutions
          </a>
          <a href={"#"} className="py-1 px-4 rounded-md hover:bg-blue-100">
            Associations
          </a>
          <a href={"#"} className="py-1 px-4 rounded-md hover:bg-blue-100">
            Actualités
          </a>
          <a href={"#"} className="py-1 px-4 rounded-md hover:bg-blue-100">
            Contact
          </a>
        </div>
        <UserMenu />
      </nav>
    </header>
  );
};
