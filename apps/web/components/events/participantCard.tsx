import { FC } from "react";

interface ParticpantCardProps {
  name: string;
  avatar?: string;
}

export const ParticipantCard: FC<ParticpantCardProps> = (props) => {
  return (
    <div className="bg-gray-50 rounded block px-3 py-2">
      <p>{props.name}</p>
    </div>
  );
};
