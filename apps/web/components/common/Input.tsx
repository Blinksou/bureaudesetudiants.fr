interface Input {
  value: string | number;
  onchange: () => void;
  id?: string;
  name?: string;
  classes?: string;
  autoComplete?: string;
  type?: string;
  required?: boolean;
  placeholder?: string;
}

const Input = ({
  value,
  onchange,
  id = "",
  name = "",
  classes = "",
  autoComplete = "",
  required = true,
  type = "text",
  placeholder = "",
}: Input) => {
  return (
    <input
      id={id}
      name={name}
      value={value}
      type={type}
      className={`appearance-none px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-primary focus:border-primary sm:text-sm ${classes}`}
      onChange={onchange}
      autoComplete={autoComplete}
      required={required}
      placeholder={placeholder}
    />
  );
};

export default Input;
