interface BadgeProps {
  label: string;
  type?: "success" | "danger" | "warning";
}

const Badge = ({ label, type }: BadgeProps) => {
  return (
    <span
      className={`inline-flex rounded-full px-2 text-xs font-semibold leading-5 ${
        type == "success"
          ? "bg-green-100 text-green-800"
          : type == "danger"
          ? "bg-error text-white"
          : type == "warning"
          ? "bg-yellow-500 text-white"
          : "bg-gray-600 text-white"
      }`}
    >
      {label}
    </span>
  );
};

export default Badge;
