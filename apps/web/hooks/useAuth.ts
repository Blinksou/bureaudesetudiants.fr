import axios from "axios";
import { useCallback, useEffect, useState } from "react";
import { api } from "../components/common/api";

export interface IUserDto {
  userId: string;
  email: string;
  isAdmin: boolean;
  profile?: string;
  token: string;
  updatedAt: Date;
  isLoggedIn: boolean;
}

export const useAuth = () => {
  const [userId, setUserId] = useState("");
  const [email, setEmail] = useState("");
  const [isAdmin, setIsAdmin] = useState(false);
  const [profile, setProfile] = useState(null);
  const [token, setToken] = useState("");
  const [updatedAt, setUpdatedAt] = useState(new Date(1900, 0, 1));
  let isLoggedIn = false;

  if (typeof window !== "undefined") {
    isLoggedIn = localStorage.getItem("userData") !== null;
  }

  const login = useCallback((data: IUserDto) => {
    const { userId, email, isAdmin, profile, token, updatedAt } = data;
    setUserId(userId);
    setEmail(email);
    setIsAdmin(isAdmin);
    setProfile(profile);
    setToken(token);
    setUpdatedAt(updatedAt);

    localStorage.setItem(
      "userData",
      JSON.stringify({
        userId,
        email,
        isAdmin,
        profile,
        token,
      })
    );

    axios.defaults.headers["Authorization"] = `Bearer ${token}`;
    api.defaults.headers["Authorization"] = `Bearer ${token}`;
  }, []);

  const logout = useCallback(() => {
    setUserId("");
    setEmail("");
    setIsAdmin(false);
    setProfile(null);
    setToken("");
    setUpdatedAt(new Date(1900, 0, 1));
    isLoggedIn = false;
    localStorage.removeItem("userData");
  }, []);

  useEffect(() => {
    const storedData = JSON.parse(localStorage.getItem("userData"));
    if (storedData && storedData.token) {
      login(storedData);
    }
  }, [login]);

  return {
    userId,
    email,
    isAdmin,
    profile,
    token,
    updatedAt,
    login,
    logout,
    isLoggedIn,
  };
};
