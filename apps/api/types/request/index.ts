import { Request } from 'express';
import { UserWithProfile } from '../../lib/user/types';

export interface RequestWithUser extends Request {
  user: UserWithProfile;
}
