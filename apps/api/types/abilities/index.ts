import { PrismaAbility, Subjects } from '@casl/prisma';
import { Association, Profile, User } from 'prisma-types';

export enum Action {
  Manage = 'manage', // everything
  Create = 'create',
  Read = 'read',
  Update = 'update',
  Delete = 'delete',
}

export type ModelSubjects = {
  User: User;
  UserProfile: Profile;
  Association: Association;
};

export type AppAbility = PrismaAbility<[string, Subjects<ModelSubjects>]>;
