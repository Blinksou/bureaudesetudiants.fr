import { Prisma } from 'prisma-types';

export interface Config {
  uploads: UploadConfig;
  sentry: SentryConfig;
  database: DatabaseConfig;
}

export interface DatabaseConfig {
  seed: {
    models: Lowercase<Prisma.ModelName>[];
  };
}

export interface SentryConfig {
  dsn: string;
}

export interface UploadConfig {
  fileDestination: string;
}
