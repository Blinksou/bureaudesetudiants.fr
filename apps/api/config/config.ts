import { Config } from './config.interface';

export default (): Config => ({
  database: {
    seed: {
      models: ['user', 'association', 'event', 'news'],
    },
  },
  uploads: {
    fileDestination: process.env.UPLOADED_FILES_DESTINATION || './uploads',
  },
  sentry: {
    dsn: process.env.SENTRY_DSN || '',
  },
});
