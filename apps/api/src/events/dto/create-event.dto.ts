import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDate,
  IsNumber,
  IsString,
  ValidateIf,
} from 'class-validator';
import { Transform, Type } from 'class-transformer';

export class CreateEventDto {
  @ApiProperty()
  @IsString()
  name!: string;

  @ApiProperty()
  @IsString()
  description!: string;

  @ApiProperty()
  @IsNumber()
  @ValidateIf((object, value) => value)
  @Transform(({ value }) => parseInt(value))
  price?: number | null;

  @ApiProperty()
  @IsBoolean()
  isPublic!: boolean;

  @ApiProperty()
  @IsDate()
  @Type(() => Date)
  startDate!: Date;

  @ApiProperty()
  @IsDate()
  @ValidateIf((object, value) => value !== undefined)
  @Type(() => Date)
  endDate?: Date | null;
}
