import { Injectable } from '@nestjs/common';
import { CreateEventDto } from './dto/create-event.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { PrismaService } from '../prisma/prisma.service';
import { User } from 'prisma-types';

@Injectable()
export class EventsService {
  constructor(private readonly prisma: PrismaService) {}

  async create(createEventDto: CreateEventDto, associationId: string) {
    return this.prisma.event.create({
      data: {
        ...createEventDto,
        associationId,
      },
    });
  }

  async findAll() {
    return this.prisma.event.findMany({});
  }

  async findAllByAssociation(associationId: string) {
    return this.prisma.event.findMany({ where: { associationId } });
  }

  async isUserSubscribedToEvent(user: User, eventId: string) {
    const userEventExist = await this.prisma.userOnEvent.findFirst({
      where: { userId: user.id, eventId },
    });

    if (userEventExist) {
      return true;
    }

    return false;
  }

  async subscribeUserToEvent(user: User, eventId: string) {
    const userEventExist = await this.prisma.userOnEvent.findFirst({
      where: {
        userId: user.id,
        eventId,
      },
    });

    if (userEventExist) {
      return userEventExist;
    }

    return this.prisma.userOnEvent.create({
      data: {
        eventId,
        userId: user.id,
        hasParticipated: false,
      },
    });
  }

  async unsubscribeUserFromEvent(user: User, eventId: string) {
    const userEventExist = await this.prisma.userOnEvent.findFirst({
      where: {
        userId: user.id,
        eventId,
      },
    });

    if (!userEventExist) {
      return;
    }

    return this.prisma.userOnEvent.delete({
      where: {
        userId_eventId: {
          eventId,
          userId: user.id,
        },
      },
    });
  }

  async findOne(id: string) {
    return this.prisma.event.findFirst({
      where: { id },
      include: { users: { include: { user: { include: { profile: true } } } } },
    });
  }

  async update(id: string, updateEventDto: UpdateEventDto) {
    return this.prisma.event.update({ where: { id }, data: updateEventDto });
  }

  async remove(id: string) {
    return this.prisma.event.delete({ where: { id } });
  }
}
