import { Test, TestingModule } from '@nestjs/testing';
import { EventsService } from './events.service';
import { PrismaService } from 'src/prisma/prisma.service';
import { Event, User, UserOnEvent } from 'prisma-types';

const user: User = {
  id: 'cl0ux4rjh000009l034o3be5z',
  email: 'hello@world.com',
  password: 'mysuperpass',
  isAdmin: false,
  createdAt: new Date(),
  updatedAt: new Date(),
};
const eventsArray: Event[] = [
  {
    id: 'a cuid',
    name: 'a name',
    description: 'a description',
    price: 100,
    isPublic: true,
    startDate: new Date(),
    endDate: new Date(),
    associationId: 'an asso id',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    id: 'a cuid 2',
    name: 'a name 2',
    description: 'a description 2',
    price: 100,
    isPublic: true,
    startDate: new Date(),
    endDate: new Date(),
    associationId: 'an asso id 2',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
];
const userOnEventArray: UserOnEvent[] = [
  {
    userId: 'a user id',
    eventId: 'a event id',
    hasParticipated: false,
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    userId: 'a user id 2',
    eventId: 'a event id 2',
    hasParticipated: false,
    createdAt: new Date(),
    updatedAt: new Date(),
  },
];
const oneUserOnEvent = userOnEventArray[0];
const oneEvent = eventsArray[0];

const db = {
  event: {
    create: jest.fn().mockReturnValue(oneEvent),
    findMany: jest.fn().mockResolvedValue(eventsArray),
    findFirst: jest.fn().mockResolvedValue(oneEvent),
    delete: jest.fn().mockResolvedValue(oneEvent),
    update: jest.fn().mockResolvedValue(oneEvent),
  },
  userOnEvent: {
    delete: jest.fn().mockResolvedValue(oneUserOnEvent),
    findFirst: jest.fn().mockResolvedValue(oneUserOnEvent),
    create: jest.fn().mockReturnValue(oneUserOnEvent),
  },
};

describe('EventsService', () => {
  let service: EventsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EventsService,
        {
          provide: PrismaService,
          useValue: db,
        },
      ],
    }).compile();

    service = module.get<EventsService>(EventsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create', () => {
    it('should create an event', async () => {
      const createdEvent = await service.create(
        {
          name: 'a name',
          description: 'blabla',
          isPublic: true,
          startDate: new Date(),
          endDate: new Date(),
        },
        'assoid',
      );
      expect(createdEvent).toEqual(oneEvent);
    });
  });

  describe('findAll', () => {
    it('should get all events', () => {
      expect(service.findAll()).resolves.toEqual(eventsArray);
    });
  });

  describe('findAllByAssociation', () => {
    it('should get all events by associationId', () => {
      expect(service.findAllByAssociation('assoId')).resolves.toEqual(
        eventsArray,
      );
    });
  });

  describe('isUserSubscribedToEvent', () => {
    it('should return true', () => {
      expect(service.isUserSubscribedToEvent(user, 'eventId'));
    });
  });

  describe('subscribeUserToEvent', () => {
    it('should subscribe user to event', () => {
      expect(service.subscribeUserToEvent(user, 'eventId')).resolves.toEqual(
        oneUserOnEvent,
      );
    });
  });

  describe('unsubscribeUserFromEvent', () => {
    it('should unsubscribe user to event', () => {
      expect(
        service.unsubscribeUserFromEvent(user, 'eventid'),
      ).resolves.toEqual(oneUserOnEvent);
    });
  });

  describe('findOne', () => {
    it('should return an event', () => {
      expect(service.findOne('id')).resolves.toEqual(oneEvent);
    });
  });

  describe('update', () => {
    it('should update an event', () => {
      expect(service.update('id', { name: 'lol' })).resolves.toEqual(oneEvent);
    });
  });

  describe('remove', () => {
    it('should delete an event', () => {
      expect(service.remove('id')).resolves.toEqual(oneEvent);
    });
  });
});
