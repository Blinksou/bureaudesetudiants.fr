import {
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Request,
  Res,
} from '@nestjs/common';
import { EventsService } from './events.service';
import { CreateEventDto } from './dto/create-event.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { RequestWithUser } from '../../types/request';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { AssociationUserService } from '../association-user/association-user.service';
import { Role } from 'prisma-types';
import { Response } from 'express';

@ApiTags('Events')
@Controller()
export class EventsController {
  constructor(
    private readonly eventsService: EventsService,
    private readonly associationUserService: AssociationUserService,
  ) {}

  @Post('associations/:associationId/events')
  @ApiResponse({ status: 401 })
  @ApiResponse({ status: 201 })
  async create(
    @Request() req: RequestWithUser,
    @Param('associationId') associationId: string,
    @Body() createEventDto: CreateEventDto,
  ) {
    const member = await this.associationUserService.getAssociationUser({
      associationId,
      userId: req.user.id,
    });

    const allowedRoles: Array<Role> = [
      Role.PRESIDENT,
      Role.COMMUNITY_MANAGER,
      Role.EVENT_MANAGER,
    ];

    if ((!member || !allowedRoles.includes(member.role)) && !req.user.isAdmin) {
      throw new ForbiddenException();
    }

    return this.eventsService.create(createEventDto, associationId);
  }

  @Get('events')
  @ApiResponse({ status: 401 })
  @ApiResponse({ status: 200 })
  async findAll(@Request() req: RequestWithUser) {
    if (!req.user.isAdmin) {
      throw new ForbiddenException();
    }

    return this.eventsService.findAll();
  }

  @Get('associations/:associationId/events')
  async findAllByAssociation(@Param('associationId') associationId: string) {
    return this.eventsService.findAllByAssociation(associationId);
  }

  @Get('events/:id')
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 404 })
  async findOne(@Param('id') id: string) {
    const news = this.eventsService.findOne(id);

    if (!news) {
      throw new NotFoundException();
    }

    return news;
  }

  @Get('events/:eventId/isSubscribed')
  @ApiResponse({ status: 200 })
  async isUserSubscribedToEvent(
    @Param('eventId') eventId: string,
    @Request() req: RequestWithUser,
  ) {
    return this.eventsService.isUserSubscribedToEvent(req.user, eventId);
  }

  @Get('events/:eventId/subscribe')
  @ApiResponse({ status: 204 })
  async subscribeUserToEvent(
    @Res() res: Response,
    @Param('eventId') eventId: string,
    @Request() req: RequestWithUser,
  ) {
    await this.eventsService.subscribeUserToEvent(req.user, eventId);

    return res.status(204).send();
  }

  @Get('events/:eventId/unsubscribe')
  @ApiResponse({ status: 204 })
  async unsubscribeUserFromEvent(
    @Res() res: Response,
    @Param('eventId') eventId: string,
    @Request() req: RequestWithUser,
  ) {
    await this.eventsService.unsubscribeUserFromEvent(req.user, eventId);

    return res.status(204).send();
  }

  @Patch('events/:id')
  @ApiResponse({ status: 401 })
  @ApiResponse({ status: 200 })
  async update(
    @Request() req: RequestWithUser,
    @Param('id') id: string,
    @Body() updateEventDto: UpdateEventDto,
  ) {
    const event = await this.eventsService.findOne(id);

    if (!event) {
      throw new NotFoundException();
    }

    const member = await this.associationUserService.getAssociationUser({
      associationId: event.associationId,
      userId: req.user.id,
    });

    const allowedRoles: Array<Role> = [
      Role.PRESIDENT,
      Role.COMMUNITY_MANAGER,
      Role.EVENT_MANAGER,
    ];

    if ((!member || !allowedRoles.includes(member.role)) && !req.user.isAdmin) {
      throw new ForbiddenException();
    }

    return this.eventsService.update(id, updateEventDto);
  }

  @Delete('events/:id')
  @ApiResponse({ status: 200 })
  async remove(@Request() req: RequestWithUser, @Param('id') id: string) {
    const event = await this.eventsService.findOne(id);

    if (!event) {
      throw new NotFoundException();
    }

    const member = await this.associationUserService.getAssociationUser({
      associationId: event.associationId,
      userId: req.user.id,
    });

    const allowedRoles: Array<Role> = [
      Role.PRESIDENT,
      Role.COMMUNITY_MANAGER,
      Role.EVENT_MANAGER,
    ];

    if ((!member || !allowedRoles.includes(member.role)) && !req.user.isAdmin) {
      throw new ForbiddenException();
    }

    await this.eventsService.remove(id);

    return { message: 'Ok' };
  }
}
