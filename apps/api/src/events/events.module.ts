import { Module } from '@nestjs/common';
import { EventsService } from './events.service';
import { EventsController } from './events.controller';
import { PrismaModule } from '../prisma/prisma.module';
import { AssociationUserModule } from '../association-user/association-user.module';

@Module({
  controllers: [EventsController],
  providers: [EventsService],
  imports: [PrismaModule, AssociationUserModule],
})
export class EventsModule {}
