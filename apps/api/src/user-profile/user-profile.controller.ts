import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  Post,
  Request,
  Res,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { UserProfileService } from './user-profile.service';
import { CreateUserProfileDto } from './dto/create-user-profile.dto';
import { UpdateUserProfileDto } from './dto/update-user-profile.dto';
import { ApiTags } from '@nestjs/swagger';
import { RequestWithUser } from '../../types/request';
import { Action } from '../../types/abilities';
import { UserProfileAbilities } from './user-profile.abilities';
import { LocalFilesInterceptor } from '../upload/local-files.interceptor';
import { Public } from '../auth/decorator/public.decorator';
import { Response } from 'express';
import { ConfigService } from '@nestjs/config';
import { FindUserProfileDto } from './dto/find-user-profile.dto';

const AVATAR_PATH = '/avatars';

@ApiTags('UserProfile')
@Controller('user/profile')
export class UserProfileController {
  constructor(
    private readonly userProfileService: UserProfileService,
    private readonly userProfileAbilities: UserProfileAbilities,
    private readonly configService: ConfigService,
  ) {}

  @Post()
  @UseInterceptors(
    LocalFilesInterceptor({
      fieldName: 'avatar',
      path: AVATAR_PATH,
    }),
  )
  async create(
    @Res() res: Response,
    @Request() req: RequestWithUser,
    @Body() createUserProfileDto: CreateUserProfileDto,
    @UploadedFile() avatar?: Express.Multer.File,
  ) {
    try {
      const existingUserProfile = await this.userProfileService.findOne({
        email: req.user.email,
      });

      if (existingUserProfile) {
        throw new BadRequestException('User profile already exists');
      }

      const userProfile = await this.userProfileService.create(
        req.user,
        createUserProfileDto,
        avatar,
      );

      return res.json({
        data: userProfile,
      });
    } catch (e: any) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        error: e.message,
      });
    }
  }

  @Get()
  async findAll(@Res() res: Response, @Request() req: RequestWithUser) {
    try {
      this.userProfileAbilities.throwUnlessAllowed(Action.Manage, req.user);

      const users = await this.userProfileService.findAll();

      return res.json({
        data: {
          users,
        },
      });
    } catch (e: any) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        error: e.message,
      });
    }
  }

  @Get(':id')
  async findOne(
    @Res() res: Response,
    @Body() findUserProfileDto: FindUserProfileDto,
  ) {
    try {
      const user = await this.userProfileService.findOne(findUserProfileDto);

      return res.json({
        data: {
          user,
        },
      });
    } catch (e: any) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        error: e.message,
      });
    }
  }

  @Patch(':id')
  @UseInterceptors(
    LocalFilesInterceptor({
      fieldName: 'avatar',
      path: AVATAR_PATH,
    }),
  )
  async update(
    @Res() res: Response,
    @Request() req: RequestWithUser,
    @Body() updateUserProfileDto: UpdateUserProfileDto,
    @Param('id') id?: string,
    @UploadedFile() avatar?: Express.Multer.File,
  ) {
    try {
      this.userProfileAbilities.throwUnlessAllowed(Action.Update, req.user, id);

      const updatedUser = await this.userProfileService.update(
        id ?? req.user.profile.id,
        updateUserProfileDto,
        avatar,
      );

      return res.json({
        data: updatedUser,
      });
    } catch (e: any) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        error: e.message,
      });
    }
  }

  @Delete(':id')
  async remove(
    @Res() res: Response,
    @Request() req: RequestWithUser,
    @Param('id') id: string,
  ) {
    try {
      this.userProfileAbilities.throwUnlessAllowed(Action.Manage, req.user);

      await this.userProfileService.remove(id);

      return res.json({ message: 'User deleted' });
    } catch (e: any) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        error: e.message,
      });
    }
  }

  @Public()
  @Get('/avatar/:avatarId')
  async getAvatar(@Param('avatarId') id: string, @Res() res: Response) {
    res.sendFile(id, {
      root: `${this.configService.get(
        'UPLOADED_FILES_DESTINATION',
      )}${AVATAR_PATH}`,
    });
  }
}
