import { Module } from '@nestjs/common';
import { UserProfileService } from './user-profile.service';
import { UserProfileController } from './user-profile.controller';
import { UserProfileAbilities } from './user-profile.abilities';
import { ConfigService } from '@nestjs/config';

@Module({
  controllers: [UserProfileController],
  providers: [UserProfileService, UserProfileAbilities, ConfigService],
  exports: [UserProfileService],
})
export class UserProfileModule {}
