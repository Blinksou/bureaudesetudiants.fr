import { User } from 'prisma-types';
import { AbilityBuilder, AbilityClass, ForbiddenError } from '@casl/ability';
import { PrismaAbility } from '@casl/prisma';
import { Action, AppAbility } from '../../types/abilities';

export class UserProfileAbilities {
  private static defineAbility(user: User, userProfileIdToCompare?: string) {
    const { can, build } = new AbilityBuilder(
      PrismaAbility as AbilityClass<AppAbility>,
    );

    can(Action.Read, 'UserProfile');

    if (user.isAdmin) {
      can(Action.Manage, 'UserProfile');
    }

    if (
      user.isAdmin ||
      (userProfileIdToCompare && userProfileIdToCompare === user.id)
    ) {
      can(Action.Update, 'UserProfile');
    }

    return build();
  }

  public throwUnlessAllowed(
    action: Action,
    user: User,
    userProfileIdToCompare?: string,
  ) {
    ForbiddenError.from(
      UserProfileAbilities.defineAbility(user, userProfileIdToCompare),
    )
      .setMessage(`Not allowed to ${action}`)
      .throwUnlessCan(action, 'UserProfile');
  }
}
