import { Test, TestingModule } from '@nestjs/testing';
import { UserProfileService } from './user-profile.service';

import { PrismaService } from 'src/prisma/prisma.service';
import { Profile, User } from 'prisma-types';
import { CreateUserProfileDto } from './dto/create-user-profile.dto';

jest.setTimeout(30000);

const userProfileDto: CreateUserProfileDto = {
  firstname: 'john',
  lastname: 'doe',
  phone: '+33611223344',
};
const user: User = {
  id: 'cl0ux4rjh000009l034o3be5z',
  email: 'hello@world.com',
  password: 'mysuperpass',
  isAdmin: false,
  createdAt: new Date(),
  updatedAt: new Date(),
};
const userProfileArray: Profile[] = [
  {
    id: 'my id',
    firstname: 'john',
    lastname: 'doe',
    avatar: 'imgpath',
    phone: '+33611223344',
    userId: 'cl0ux4rjh000009l034o3be5z',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    id: 'my id 2',
    firstname: 'matt',
    lastname: 'lac',
    avatar: 'imgpath',
    phone: '+33611221144',
    userId: 'mapekfde',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
];
const oneUserProfile = userProfileArray[0];

const db = {
  profile: {
    create: jest.fn().mockReturnValue(oneUserProfile),
    findMany: jest.fn().mockResolvedValue(userProfileArray),
    findFirst: jest.fn().mockResolvedValue(oneUserProfile),
    update: jest.fn().mockResolvedValue(oneUserProfile),
    delete: jest.fn().mockResolvedValue(oneUserProfile),
  },
};

describe('UserProfileService', () => {
  let service: UserProfileService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserProfileService,
        {
          provide: PrismaService,
          useValue: db,
        },
      ],
    }).compile();

    service = module.get<UserProfileService>(UserProfileService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create', () => {
    it('should create a new user profile', async () => {
      const createdUser = await service.create(user, userProfileDto);
      expect(createdUser).toEqual(oneUserProfile);
    });
  });

  describe('findAll', () => {
    it('should get all user profile', () => {
      expect(service.findAll()).resolves.toEqual(userProfileArray);
    });
  });

  describe('findOne', () => {
    it('shoud get a user profile', async () => {
      expect(service.findOne({ id: 'bffbe' })).resolves.toEqual(oneUserProfile);
      expect(service.findOne({ email: 'bffbe@lin.com' })).resolves.toEqual(
        oneUserProfile,
      );
    });
  });

  describe('update', () => {
    it('should update a user profile', async () => {
      const updatedUser = await service.update('my user id', {
        firstname: 'alex',
        lastname: 'dez',
      });
      expect(updatedUser).toEqual(oneUserProfile);
    });
  });

  describe('remove', () => {
    it('should delete a user profile', () => {
      expect(service.remove('a cuid')).resolves.toEqual(oneUserProfile);
    });
  });
});
