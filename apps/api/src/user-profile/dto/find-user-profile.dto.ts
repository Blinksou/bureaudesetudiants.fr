import { IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class FindUserProfileDto {
  @ApiProperty()
  @IsString()
  email?: string;

  @ApiProperty()
  @IsString()
  id?: string;
}
