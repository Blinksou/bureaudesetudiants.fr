import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsPhoneNumber, IsString } from 'class-validator';

export class CreateUserProfileDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  firstname!: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  lastname!: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsPhoneNumber('FR')
  phone!: string;
}
