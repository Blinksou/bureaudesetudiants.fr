import { Exclude } from 'class-transformer';

export class ProfileDto {
  firstname!: string;
  lastname!: string;
  avatar?: string;
  phone?: string;

  @Exclude()
  userId!: string;

  createdAt!: Date;
  updatedAt!: Date;
}
