import { Injectable } from '@nestjs/common';
import { CreateUserProfileDto } from './dto/create-user-profile.dto';
import { UpdateUserProfileDto } from './dto/update-user-profile.dto';
import { PrismaService } from '../prisma/prisma.service';
import { Prisma, User } from 'prisma-types';
import { FindUserProfileDto } from './dto/find-user-profile.dto';
import ProfileUpdateInput = Prisma.ProfileUpdateInput;

@Injectable()
export class UserProfileService {
  constructor(private readonly prisma: PrismaService) {}

  async create(
    user: User,
    createUserProfileDto: CreateUserProfileDto,
    file?: Express.Multer.File,
  ) {
    return this.prisma.profile.create({
      data: {
        ...createUserProfileDto,
        userId: user.id,
        avatar: file?.filename,
      },
    });
  }

  async findAll() {
    return this.prisma.profile.findMany();
  }

  async findOne(findUserProfileDto: FindUserProfileDto) {
    return this.prisma.profile.findFirst({
      where: {
        user: findUserProfileDto,
      },
    });
  }

  async update(
    id: string,
    updateUserProfileDto: UpdateUserProfileDto,
    file?: Express.Multer.File,
  ) {
    const data: ProfileUpdateInput = {
      ...updateUserProfileDto,
    };

    if (file) {
      data.avatar = file.filename;
    }

    return this.prisma.profile.update({
      data,
      where: { id },
    });
  }

  async remove(id: string) {
    return this.prisma.profile.delete({ where: { id } });
  }

  async getMyProfile(user: User) {
    return this.prisma.profile.findFirst({
      where: {
        userId: user.id,
      },
    });
  }
}
