import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  HttpStatus,
  NotFoundException,
  Param,
  Post,
  Put,
  Request,
  Res,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { CurrentUser } from 'src/auth/decorator/user.decorator';
import { Payload } from 'src/auth/jwt/types';

import { AssociationService } from './association.service';
import { CreateAssociationDto } from './dto/create-association.dto';
import { UpdateAssociationDto } from './dto/update-association.dto';
import { GetAssociationFilter } from './filter/decorator';
import { AssociationFilter } from './filter/types';
import { Response } from 'express';
import { AssociationAbilities } from './association.abilities';
import { Action } from '../../types/abilities';
import { RequestWithUser } from '../../types/request';
import { UpdateAssociationStatusDto } from './dto/update-association-status.dto';
import { Role } from 'prisma-types';

@ApiTags('Association')
@Controller('associations')
export class AssociationController {
  constructor(
    private associationService: AssociationService,
    private associationAbilities: AssociationAbilities,
  ) {}

  @Get()
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 400 })
  @ApiResponse({ status: 401 })
  async findAll(
    @Res() res: Response,
    @Request() req: RequestWithUser,
    @GetAssociationFilter() filters: AssociationFilter,
  ) {
    try {
      this.associationAbilities.throwUnlessAllowed(Action.Read, req.user);

      const associations =
        await this.associationService.findAllAssociationsFiltered(filters);

      return res.json(associations);
    } catch (e: any) {
      return res
        .status(e.code ?? HttpStatus.BAD_REQUEST)
        .json({ error: e.message });
    }
  }

  @Get(':associationId')
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 400 })
  @ApiResponse({ status: 401, description: "Association doesn't exists." })
  async association(
    @Request() req: RequestWithUser,
    @Param('associationId') associationId: string,
  ) {
    try {
      const association = await this.associationService.getAssociation({
        id: associationId,
      });

      if (!association) {
        throw new NotFoundException("Association doesn't exists.");
      }

      return { data: association };
    } catch (err: any) {
      throw new BadRequestException(err?.message || 'Unknown error.');
    }
  }

  @Post()
  @ApiResponse({
    status: 201,
    description: 'Association has been successfully created.',
  })
  @ApiResponse({ status: 400, description: 'Bad request.' })
  async createAssociation(
    @Res() res: Response,
    @Body() createAssociationDto: CreateAssociationDto,
    @CurrentUser() user: Payload,
  ) {
    try {
      const createdAssociation =
        await this.associationService.createAssociation(
          createAssociationDto,
          user.email,
        );

      return res.json({ data: createdAssociation });
    } catch (e: any) {
      return res
        .status(e.code ?? HttpStatus.BAD_REQUEST)
        .json({ error: e.message });
    }
  }

  @Put(':associationId')
  @ApiResponse({
    status: 200,
    description: 'Association has been successfully updated.',
  })
  @ApiResponse({ status: 400, description: 'Bad request.' })
  @ApiResponse({ status: 401 })
  async updateAssociation(
    @Request() req: RequestWithUser,
    @Res() res: Response,
    @Param('associationId') associationId: string,
    @Body() associationDto: UpdateAssociationDto,
  ) {
    try {
      const association = await this.associationService.getAssociation({
        id: associationId,
      });

      if (!association) {
        throw new NotFoundException("Association doesn't exists.");
      }

      let isAllowed = false;
      for (const member of association.users) {
        if (member.user.id === req.user.id && member.role === Role.PRESIDENT) {
          isAllowed = true;
        }
      }

      if (!isAllowed) {
        throw new ForbiddenException("You don't have permission to do this.");
      }

      const updatedAssociation =
        await this.associationService.updateAssociation(
          associationId,
          associationDto,
        );

      return res.json({ data: updatedAssociation });
    } catch (e: any) {
      return res
        .status(e.code ?? HttpStatus.BAD_REQUEST)
        .json({ error: e.message });
    }
  }

  @Put(':associationId/status')
  @ApiResponse({
    status: 200,
    description: 'Association status has been successfully updated.',
  })
  @ApiResponse({ status: 400, description: 'Bad request.' })
  async updateAssociationStatus(
    @Request() req: RequestWithUser,
    @Res() res: Response,
    @Param('associationId') associationId: string,
    @Body() updateAssociationStatusDto: UpdateAssociationStatusDto,
  ) {
    try {
      this.associationAbilities.throwUnlessAllowed(Action.Update, req.user);

      const updatedAssociation =
        await this.associationService.updateAssociation(associationId, {
          status: updateAssociationStatusDto.status,
        });

      return res.json({ data: updatedAssociation });
    } catch (e: any) {
      return res
        .status(e.code ?? HttpStatus.BAD_REQUEST)
        .json({ error: e.message });
    }
  }

  @Delete(':associationId')
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 400 })
  async deleteAssociation(
    @Request() req: RequestWithUser,
    @Param('associationId') associationId: string,
  ) {
    try {
      const association = await this.associationService.getAssociation({
        id: associationId,
      });

      if (!association) {
        throw new NotFoundException("Association doesn't exists.");
      }

      let isAllowed = false;
      for (const member of association.users) {
        if (member.user.id === req.user.id && member.role === Role.PRESIDENT) {
          isAllowed = true;
        }
      }

      if (!isAllowed) {
        throw new ForbiddenException("You don't have permission to do this.");
      }

      const deletedAssociation =
        await this.associationService.deleteAssociation({ id: associationId });

      return { data: deletedAssociation };
    } catch (err: any) {
      throw new BadRequestException(err?.message || 'Unknown error.');
    }
  }
}
