import { Test, TestingModule } from '@nestjs/testing';
import cuid from 'cuid';
import { Association } from 'prisma-types';
import { PrismaService } from 'src/prisma/prisma.service';

import { AssociationService } from './association.service';

jest.setTimeout(30000);

const associationArray: Association[] = [
  {
    id: cuid(),
    name: 'Ma super asso',
    description: 'Description de mon asso',
    address1: '89 Quai des Chartrons',
    address2: null,
    city: 'Bordeaux',
    zipCode: '33000',
    country: 'France',
    status: 'WAITING_APPROVAL',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    id: cuid(),
    name: 'Ynov BDE',
    description: 'Description de mon asso',
    address1: '5 rue de Condé',
    address2: null,
    city: 'Bordeaux',
    zipCode: '33000',
    country: 'France',
    status: 'WAITING_APPROVAL',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
];
const oneAssociation = associationArray[0];
const db = {
  association: {
    findMany: jest.fn().mockResolvedValue(associationArray),
    create: jest.fn().mockReturnValue(oneAssociation),
    findUnique: jest.fn().mockResolvedValue(oneAssociation),
    update: jest.fn().mockResolvedValue(oneAssociation),
    delete: jest.fn().mockResolvedValue(oneAssociation),
  },
};

describe('AssociationService', () => {
  let service: AssociationService;
  let prisma: PrismaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AssociationService,
        {
          provide: PrismaService,
          useValue: db,
        },
      ],
    }).compile();

    service = module.get<AssociationService>(AssociationService);
    prisma = module.get<PrismaService>(PrismaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getAssociation', () => {
    it('should get a single association', () => {
      expect(
        service.getAssociation({ id: oneAssociation.id }),
      ).resolves.toEqual(oneAssociation);
    });
  });

  describe('createAssociation', () => {
    it('should create a new association', () => {
      expect(
        service.createAssociation(
          {
            name: oneAssociation.name,
            description: oneAssociation.description,
            address1: oneAssociation.address1,
            city: oneAssociation.city,
            zipCode: oneAssociation.zipCode,
            country: oneAssociation.country,
          },
          'test@test.com',
        ),
      ).resolves.toEqual(oneAssociation);
    });
  });

  describe('updateAssociation', () => {
    it('should call the update method', async () => {
      const association = await service.updateAssociation(oneAssociation.id, {
        name: oneAssociation.name,
        description: oneAssociation.description,
      });

      expect(association).toEqual(oneAssociation);
    });
  });

  describe('deleteAssociation', () => {
    it('should return the deleted association', () => {
      expect(
        service.deleteAssociation({ id: oneAssociation.id }),
      ).resolves.toEqual(oneAssociation);
    });
  });

  describe('findAllAssociationsFiltered', () => {
    it('should return an array of associations', () => {
      expect(
        service.findAllAssociationsFiltered({ status: 'WAITING_APPROVAL' }),
      ).resolves.toEqual(associationArray);
    });
  });
});
