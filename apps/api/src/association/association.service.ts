import { Injectable } from '@nestjs/common';
import { Prisma } from 'prisma-types';

import { PrismaService } from 'src/prisma/prisma.service';
import { CreateAssociationDto } from './dto/create-association.dto';
import { AssociationFilter } from './filter/types';
import { parseAssociationFilter } from './filter';

@Injectable()
export class AssociationService {
  constructor(private prisma: PrismaService) {}

  async createAssociation(
    createAssociationDto: CreateAssociationDto,
    userEmail: string,
  ) {
    return this.prisma.association.create({
      data: {
        ...createAssociationDto,
        users: {
          create: {
            role: 'PRESIDENT',
            user: {
              connect: {
                email: userEmail,
              },
            },
          },
        },
      },
    });
  }

  async getAssociation(where: Prisma.AssociationWhereUniqueInput) {
    return this.prisma.association.findUnique({
      where,
      include: {
        users: {
          include: {
            user: {
              include: {
                profile: true,
              },
            },
          },
        },
      },
    });
  }

  async deleteAssociation(where: Prisma.AssociationWhereUniqueInput) {
    return this.prisma.association.delete({
      where,
    });
  }

  async updateAssociation(
    associationId: string,
    newData: Prisma.AssociationUpdateInput,
  ) {
    return this.prisma.association.update({
      where: { id: associationId },
      data: newData,
    });
  }

  async findAllAssociationsFiltered(filters: AssociationFilter) {
    return this.prisma.association.findMany({
      where: parseAssociationFilter({}, filters),
    });
  }
}
