import { Module } from '@nestjs/common';

import { AssociationController } from './association.controller';
import { AssociationService } from './association.service';
import { AssociationAbilities } from './association.abilities';

@Module({
  controllers: [AssociationController],
  providers: [AssociationService, AssociationAbilities],
})
export class AssociationModule {}
