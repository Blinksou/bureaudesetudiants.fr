import { IsEnum, IsNotEmpty, IsString } from 'class-validator';
import { AssociationStatus } from 'prisma-types';

export class UpdateAssociationStatusDto {
  @IsString()
  @IsEnum(AssociationStatus)
  @IsNotEmpty()
  status!: AssociationStatus;
}
