import { AssociationStatus } from 'prisma-types';

export type AssociationFilter = {
  name?: string;
  status?: AssociationStatus;
};
