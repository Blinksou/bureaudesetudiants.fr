import { AssociationFilter } from './types';
import { Prisma } from 'prisma-types';
import AssociationWhereInput = Prisma.AssociationWhereInput;

export function parseAssociationFilter(
  whereParams: AssociationWhereInput,
  filter: AssociationFilter,
) {
  if (filter.name) {
    whereParams.name = {
      contains: filter.name,
    };
  }

  if (filter.status) {
    whereParams.status = {
      equals: filter.status,
    };
  }

  return whereParams;
}
