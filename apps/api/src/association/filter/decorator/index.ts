import {
  BadRequestException,
  createParamDecorator,
  ExecutionContext,
} from '@nestjs/common';
import { Request } from 'express';

import { AssociationFilter } from '../types/';
import { AssociationStatus } from 'prisma-types';

export const GetAssociationFilter = createParamDecorator(
  (data, ctx: ExecutionContext): AssociationFilter => {
    const req: Request = ctx.switchToHttp().getRequest();

    const filters: AssociationFilter = {};

    filters.name = req.query.name ? req.query.name.toString() : undefined;

    if (req.query.status) {
      if (
        !Object.values(AssociationStatus).includes(
          req.query.status as AssociationStatus,
        )
      ) {
        throw new BadRequestException('Status not found');
      }
      filters.status = req.query.status as AssociationStatus;
    }

    return filters;
  },
);
