import { User } from 'prisma-types';
import { AbilityBuilder, AbilityClass, ForbiddenError } from '@casl/ability';
import { PrismaAbility } from '@casl/prisma';
import { Action, AppAbility } from '../../types/abilities';

export class AssociationAbilities {
  private static defineAbility(user: User, associationIdToCompare?: string) {
    const { can, build } = new AbilityBuilder(
      PrismaAbility as AbilityClass<AppAbility>,
    );

    can(Action.Read, 'Association');

    if (user.isAdmin) {
      can(Action.Manage, 'Association');
    }

    if (
      user.isAdmin ||
      (associationIdToCompare && associationIdToCompare === user.id)
    ) {
      can(Action.Update, 'Association');
      can(Action.Delete, 'Association');
    }

    return build();
  }

  public throwUnlessAllowed(
    action: Action,
    user: User,
    associationIdToCompare?: string,
  ) {
    ForbiddenError.from(
      AssociationAbilities.defineAbility(user, associationIdToCompare),
    )
      .setMessage(`Not allowed to ${action}`)
      .throwUnlessCan(action, 'Association');
  }
}
