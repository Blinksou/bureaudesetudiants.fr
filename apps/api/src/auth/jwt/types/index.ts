import { Profile } from 'prisma-types';

export interface Payload {
  id: string;
  email: string;
  profile: Profile | null;
  isAdmin: boolean;
  createdAt: Date;
  updatedAt: Date;
}
