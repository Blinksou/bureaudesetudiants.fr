import { BadRequestException, Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { FindUserDto } from '../user/dto/find-user.dto';
import { verifyPassword } from '../../lib/user';
import { JwtService } from '@nestjs/jwt';
import { User } from 'prisma-types';
import { Payload } from './jwt/types';
import { UserProfileService } from '../user-profile/user-profile.service';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private userProfileService: UserProfileService,
    private jwtService: JwtService,
  ) {}

  async validateUser(findUserDto: FindUserDto, pass: string): Promise<User> {
    const user = await this.userService.validateUser(findUserDto);

    if (!(await verifyPassword(pass, user.password))) {
      throw new BadRequestException('Login / Pass did not match');
    }

    return user;
  }

  async login(user: User) {
    const profile = await this.userProfileService.getMyProfile(user);

    const payload: Payload = {
      id: user.id,
      email: user.email,
      profile: profile,
      isAdmin: user.isAdmin,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
    };

    return {
      token: this.jwtService.sign(payload),
    };
  }
}
