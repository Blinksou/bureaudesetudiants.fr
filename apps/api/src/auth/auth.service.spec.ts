import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { AuthModule } from './auth.module';
import { PrismaModule } from '../prisma/prisma.module';
import { UserModule } from '../user/user.module';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { PrismaService } from '../prisma/prisma.service';
import { UserProfileModule } from '../user-profile/user-profile.module';

jest.setTimeout(30000);

describe('AuthService', () => {
  let service: AuthService;
  let prisma: PrismaService;

  const userData = {
    id: 'a',
    email: 'test@demo.fr',
    password: 'test',
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        AuthModule,
        PrismaModule,
        UserModule,
        UserProfileModule,
        PassportModule,
        JwtModule.register({
          secret: process.env.JWT_SECRET_KEY,
          signOptions: { expiresIn: 0 },
        }),
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
    //Get a reference to the module's `PrismaService` and save it for usage in our tests.
    prisma = module.get<PrismaService>(PrismaService);

    prisma.user.create = jest.fn().mockReturnValue(userData);

    prisma.profile.findFirst = jest.fn().mockReturnValue(null);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('send a JWT token to a fresh logged in user', async () => {
    const user = await prisma.user.create({
      data: userData,
    });

    const jwtPayoad = await service.login(user);

    expect(jwtPayoad).toHaveProperty('token');

    expect(jwtPayoad.token).not.toBeNull();
  });
});
