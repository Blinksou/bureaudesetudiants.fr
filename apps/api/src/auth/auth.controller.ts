import { Body, Controller, HttpStatus, Post, Res } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { Response } from 'express';
import { UserService } from '../user/user.service';
import { Public } from './decorator/public.decorator';
import { CreateUserDto } from '../user/dto/create-user.dto';
import { plainToClass } from 'class-transformer';
import { UserDto } from '../user/dto/user.dto';

@ApiTags('Authentication')
@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
  ) {}

  @Post('login')
  @Public()
  @ApiResponse({ status: 400, description: 'Bad request' })
  async login(@Body() userDto: CreateUserDto, @Res() res: Response) {
    try {
      const user = await this.authService.validateUser(
        { email: userDto.email },
        userDto.password,
      );

      const { token } = await this.authService.login(user);

      return res.json({ token, data: plainToClass(UserDto, user) });
    } catch (e: any) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        error: e.message,
      });
    }
  }

  @Post('register')
  @Public()
  @ApiResponse({
    status: 201,
    description: 'Resource has been successfully created',
  })
  @ApiResponse({ status: 400, description: 'Bad request' })
  async register(@Body() userDto: CreateUserDto, @Res() res: Response) {
    try {
      const user = await this.userService.create(userDto);

      return res.status(HttpStatus.CREATED).json({
        data: plainToClass(UserDto, user),
      });
    } catch (e: any) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        error: e.message,
      });
    }
  }
}
