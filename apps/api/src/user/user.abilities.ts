import { User } from 'prisma-types';
import { AbilityBuilder, AbilityClass, ForbiddenError } from '@casl/ability';
import { PrismaAbility } from '@casl/prisma';
import { Action, AppAbility } from '../../types/abilities';

export class UserAbilities {
  private static defineAbility(user: User, userIdToCompare?: string) {
    const { can, build } = new AbilityBuilder(
      PrismaAbility as AbilityClass<AppAbility>,
    );

    can(Action.Read, 'User');

    if (user.isAdmin) {
      can(Action.Manage, 'User');
    }

    if (userIdToCompare && userIdToCompare === user.id) {
      can(Action.Update, 'User');
      can(Action.Delete, 'User');
    }

    return build();
  }

  public throwUnlessAllowed(
    action: Action,
    user: User,
    userIdToCompare?: string,
  ) {
    ForbiddenError.from(UserAbilities.defineAbility(user, userIdToCompare))
      .setMessage(`Not allowed to ${action}`)
      .throwUnlessCan(action, 'User');
  }
}
