import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { UserAbilities } from './user.abilities';

@Module({
  controllers: [UserController],
  providers: [UserService, UserAbilities],
  exports: [UserService],
})
export class UserModule {}
