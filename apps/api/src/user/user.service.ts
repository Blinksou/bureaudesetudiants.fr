import {BadRequestException, HttpStatus, Injectable} from '@nestjs/common';
import {UpdateUserDto} from './dto/update-user.dto';
import {PrismaService} from '../prisma/prisma.service';
import {FindUserDto} from './dto/find-user.dto';
import {EntityNotFoundInDbException} from '../prisma/exceptions/entity-not-found-in-db.exception';
import {hashPassword} from '../../lib/user';
import {CreateUserDto} from './dto/create-user.dto';

@Injectable()
export class UserService {
  constructor(private readonly prisma: PrismaService) {}

  async create(createUserDto: CreateUserDto) {
    const user = await this.findOne({ email: createUserDto.email });

    if (user) {
      throw new BadRequestException('User with this email already exists');
    }

    return this.prisma.user.create({
      data: {
        email: createUserDto.email,
        password: await hashPassword(createUserDto.password),
        isAdmin: false, // only for dev
      },
    });
  }

  async findAll() {
    return this.prisma.user.findMany({
      select: {
        password: false,

        email: true,
        createdAt: true,
        updatedAt: true,
        profile: true,
      },
    });
  }

  async findOne(findUserDto: FindUserDto) {
    return this.prisma.user.findFirst({ where: findUserDto });
  }

  async validateUser(findUserDto: FindUserDto) {
    const user = await this.prisma.user.findUnique({
      where: findUserDto,
      include: {
        profile: true,
      },
    });

    if (!user) {
      throw new EntityNotFoundInDbException(
        'User not found',
        HttpStatus.NOT_FOUND,
      );
    }

    return user;
  }

  async update(id: string, updateUserDto: UpdateUserDto) {
    if (updateUserDto.password) {
      updateUserDto.password = await hashPassword(updateUserDto.password);
    }

    return this.prisma.user.update({ data: updateUserDto, where: { id } });
  }

  async remove(id: string) {
    return this.prisma.user.delete({ where: { id } });
  }

  async getUserAssociations(id: string) {
    const associationsUser = await this.prisma.user.findFirst({
      where: {
        id,
      },
      include: {
        associations: {
          include: {
            association: true,
          },
        },
      },
    });

    return associationsUser
      ? associationsUser.associations.map(
          (association) => association.association,
        )
      : [];
  }
}
