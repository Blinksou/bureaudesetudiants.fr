import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from 'prisma-types';
import { PrismaService } from 'src/prisma/prisma.service';

jest.setTimeout(30000);

const userDto: CreateUserDto = {
  email: 'test@demo.fr',
  password: 'test',
};
const userArray: User[] = [
  {
    id: 'cl0ux4rjh000009l034o3be5z',
    email: 'hello@world.com',
    password: 'mysuperpass',
    isAdmin: false,
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    id: 'cl0uxknxu000109jjc32g61x4',
    email: 'alex@ynov.com',
    password:
      '$argon2i$v=19$m=4096,t=3,p=1$vnG6qcxwRj3aYU6f59DuKg$9OsOfLhRKX88V8+unNiIpKDGcGgJHWj+afmaH5WVEuA', // password
    isAdmin: false,
    createdAt: new Date(),
    updatedAt: new Date(),
  },
];

const oneUser = userArray[0];

const db = {
  user: {
    create: jest.fn().mockReturnValue(oneUser),
    findMany: jest.fn().mockResolvedValue(userArray),
    findFirst: jest.fn().mockResolvedValue(oneUser),
    findUnique: jest.fn().mockResolvedValue(oneUser),
    update: jest.fn().mockResolvedValue(oneUser),
    delete: jest.fn().mockResolvedValue(oneUser),
  },
};

jest.setTimeout(30000);

describe('UserService', () => {
  let service: UserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: PrismaService,
          useValue: db,
        },
      ],
    }).compile();

    service = module.get<UserService>(UserService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create', () => {
    it('should create a new user', async () => {
      db.user.findFirst = jest.fn().mockResolvedValue(null);
      const user = await service.create(userDto);
      expect(user).toEqual(oneUser);
    });
  });

  describe('findAll', () => {
    it('should get all users', () => {
      expect(service.findAll()).resolves.toEqual(userArray);
    });
  });

  describe('findOne', () => {
    it('should return a single user', async () => {
      db.user.findFirst = jest.fn().mockResolvedValue(oneUser);
      const user = await service.findOne({ email: oneUser.email });
      expect(user).toEqual(oneUser);
    });
  });

  describe('validateUser', () => {
    it('should return a valid user', () => {
      expect(service.validateUser({ email: oneUser.email })).resolves.toEqual(
        oneUser,
      );
    });
  });

  describe('update', () => {
    it('should update a user', async () => {
      const user = await service.update(oneUser.id, {
        password: 'newpass',
        email: 'test@test.com',
      });
      expect(user).toEqual(oneUser);
    });
  });

  describe('remove', () => {
    it('should remove a user', () => {
      expect(service.remove(oneUser.id)).resolves.toEqual(oneUser);
    });
  });
});
