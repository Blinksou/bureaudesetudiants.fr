import { Exclude, Type } from 'class-transformer';
import { ProfileDto } from '../../user-profile/dto/profile.dto';

export class UserDto {
  email!: string;

  @Exclude()
  password!: string;

  @Type(() => ProfileDto)
  profile?: ProfileDto | null;

  createdAt?: Date;
  updatedAt?: Date;
}
