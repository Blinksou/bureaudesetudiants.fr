import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  Post,
  Request,
  Res,
} from '@nestjs/common';
import { UserService } from './user.service';
import { UserDto } from './dto/user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { FindUserDto } from './dto/find-user.dto';
import { Response } from 'express';
import { RequestWithUser } from '../../types/request';
import { UserAbilities } from './user.abilities';
import { Action } from '../../types/abilities';
import { plainToClass } from 'class-transformer';

@ApiTags('User')
@Controller('user')
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly userAbilities: UserAbilities,
  ) {}

  @Post()
  @ApiResponse({
    status: 201,
    description: 'Resource has been successfully created',
  })
  @ApiResponse({ status: 400, description: 'Bad request' })
  async create(@Body() createUserDto: UserDto, @Res() res: Response) {
    const user = this.userService.create(createUserDto);

    return res.status(HttpStatus.CREATED).json({
      data: plainToClass(UserDto, user),
    });
  }

  @Get()
  @ApiResponse({ status: 200 })
  async findAll() {
    return this.userService.findAll();
  }

  @Get(':id')
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 404, description: 'Not found' })
  async findOne(@Body() findUserDto: FindUserDto, @Res() res: Response) {
    try {
      const user = this.userService.findOne(findUserDto);
      return res.json({
        data: plainToClass(UserDto, user),
      });
    } catch (e: any) {
      return res.status(e.code ?? HttpStatus.BAD_REQUEST).json({
        error: e.message,
      });
    }
  }

  @Patch(':id')
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 404, description: 'Not found' })
  @ApiResponse({ status: 400, description: 'Bad request' })
  async update(
    @Request() req: RequestWithUser,
    @Param('id') id: string,
    @Body() updateUserDto: UpdateUserDto,
    @Res() res: Response,
  ) {
    try {
      this.userAbilities.throwUnlessAllowed(Action.Update, req.user, id);

      const user = await this.userService.update(id, updateUserDto);

      return res.json({
        message: 'User updated',
        data: plainToClass(UserDto, user),
      });
    } catch (e: any) {
      return res.status(e.code ?? HttpStatus.BAD_REQUEST).json({
        error: e.message,
      });
    }
  }

  @Delete(':id')
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 400, description: 'Bad request' })
  async remove(
    @Request() req: RequestWithUser,
    @Param('id') id: string,
    @Res() res: Response,
  ) {
    try {
      this.userAbilities.throwUnlessAllowed(Action.Delete, req.user, id);

      await this.userService.remove(id);
      return res.status(HttpStatus.OK).json({
        data: {
          message: 'User has been removed',
        },
      });
    } catch (e: any) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        error: e.message,
      });
    }
  }

  @Get('/access/platform')
  async isAdmin(@Request() req: RequestWithUser) {
    return { isAdmin: req.user.isAdmin };
  }

  @Get('/me/associations')
  @ApiResponse({ status: 404, description: 'Not found' })
  async getUserAssociations(
    @Request() req: RequestWithUser,
    @Res() res: Response,
  ) {
    try {
      const data = await this.userService.getUserAssociations(req.user.id);
      return res.json(data);
    } catch (e: any) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        error: e.message,
      });
    }
  }
}
