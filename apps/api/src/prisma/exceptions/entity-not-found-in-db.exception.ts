import { HttpStatus } from '@nestjs/common';

export class EntityNotFoundInDbException extends Error {
  public code;

  constructor(message: string, code: HttpStatus) {
    super(message);
    this.code = code;
  }
}
