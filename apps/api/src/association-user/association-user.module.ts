import { Module } from '@nestjs/common';
import { AssociationUserController } from './association-user.controller';
import { AssociationUserService } from './association-user.service';
import { UserModule } from '../user/user.module';

@Module({
  controllers: [AssociationUserController],
  providers: [AssociationUserService],
  imports: [UserModule],
  exports: [AssociationUserService],
})
export class AssociationUserModule {}
