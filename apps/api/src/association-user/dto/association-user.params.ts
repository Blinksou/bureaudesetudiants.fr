import { ApiParam } from '@nestjs/swagger';

export class UpdateOrDeleteAssociationUserParams {
  associationId!: string;
  userId!: string;
}
