import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsEnum, IsNotEmpty } from 'class-validator';
import { Role } from 'prisma-types';

export class CreateAssociationUserDto {
  @ApiProperty()
  @IsEnum(Role)
  @IsNotEmpty()
  role: Role = 'MEMBER';

  @ApiProperty()
  @IsEmail()
  email!: string;
}
