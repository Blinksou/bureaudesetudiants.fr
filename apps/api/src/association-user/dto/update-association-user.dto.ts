import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty } from 'class-validator';
import { Role } from 'prisma-types';

export class UpdateAssociationUserDto {
  @ApiProperty()
  @IsEnum(Role)
  @IsNotEmpty()
  role!: Role;
}
