import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Request,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { AssociationUserService } from './association-user.service';
import { UpdateOrDeleteAssociationUserParams } from './dto/association-user.params';
import { CreateAssociationUserDto } from './dto/create-association-user.dto';
import { UpdateAssociationUserDto } from './dto/update-association-user.dto';
import { Public } from '../auth/decorator/public.decorator';
import { UserService } from '../user/user.service';
import { RequestWithUser } from '../../types/request';
import { Role } from 'prisma-types';

@ApiTags('AssociationUsers')
@Controller('associations/:associationId/users')
export class AssociationUserController {
  constructor(
    private readonly associationUserService: AssociationUserService,
    private readonly userService: UserService,
  ) {}

  @Get()
  @Public()
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 400 })
  @ApiResponse({ status: 401 })
  async members(@Param('associationId') associationId: string) {
    try {
      const members = await this.associationUserService.getAssociationUsers({
        associationId,
      });

      return members;
    } catch (err: any) {
      throw new BadRequestException(err?.message || 'Unknown error.');
    }
  }

  @Post('isAllowed')
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 400 })
  @ApiResponse({ status: 401 })
  async isMemberAllowed(
    @Request() req: RequestWithUser,
    @Body() body: { associationId: string; roles: Array<Role> },
  ) {
    try {
      if (!req.user) {
        return false;
      }

      if (req.user.isAdmin) {
        return true;
      }

      const member = await this.associationUserService.getAssociationUser({
        associationId: body.associationId,
        userId: req.user.id,
      });

      if (!member) {
        return false;
      }

      return body.roles.includes(member.role);
    } catch (err: any) {
      throw new BadRequestException(err?.message || 'Unknown error.');
    }
  }

  @Get('me/get')
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 400 })
  @ApiResponse({ status: 401 })
  async getCurrentAssociationUser(
    @Request() req: RequestWithUser,
    @Param('associationId') associationId: string,
  ) {
    try {
      const member = await this.associationUserService.getAssociationUser({
        associationId,
        userId: req.user.id,
      });

      return member;
    } catch (err: any) {
      throw new BadRequestException(err?.message || 'Unknown error.');
    }
  }

  @Post()
  @ApiResponse({ status: 201 })
  @ApiResponse({ status: 400 })
  @ApiResponse({ status: 401 })
  async createAssociationUser(
    @Param('associationId') associationId: string,
    @Body() createAssociationUser: CreateAssociationUserDto,
  ) {
    const user = await this.userService.validateUser({
      email: createAssociationUser.email,
    });

    return await this.associationUserService.createAssociationUser({
      associationId,
      userId: user.id,
      role: createAssociationUser.role,
    });
  }

  @Put(':userId')
  @ApiResponse({ status: 204 })
  @ApiResponse({ status: 400 })
  @ApiResponse({ status: 401 })
  async updateAssociationUser(
    @Param() params: UpdateOrDeleteAssociationUserParams,
    @Body() updateAssociationUser: UpdateAssociationUserDto,
  ) {
    return await this.associationUserService.updateAssociationUser(
      params,
      updateAssociationUser,
    );
  }

  @Delete(':userId')
  @ApiResponse({ status: 201 })
  @ApiResponse({ status: 400 })
  @ApiResponse({ status: 401 })
  async deleteAssociationUser(
    @Param() params: UpdateOrDeleteAssociationUserParams,
  ) {
    return await this.associationUserService.deleteAssociationUser(params);
  }
}
