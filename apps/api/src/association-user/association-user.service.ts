import { Injectable } from '@nestjs/common';
import { Prisma } from 'prisma-types';
import { PrismaService } from '../prisma/prisma.service';
import { UpdateOrDeleteAssociationUserParams } from './dto/association-user.params';

@Injectable()
export class AssociationUserService {
  constructor(private prisma: PrismaService) {}

  async getAssociationUsers(where: Prisma.AssociationsUsersWhereInput) {
    return this.prisma.associationsUsers.findMany({
      where,
      include: {
        user: {
          include: {
            profile: true,
          },
        },
      },
    });
  }

  async getAssociationUser(where: Prisma.AssociationsUsersWhereInput) {
    return this.prisma.associationsUsers.findFirst({ where });
  }

  async createAssociationUser(
    data: Prisma.AssociationsUsersUncheckedCreateInput,
  ) {
    const exists = await this.getAssociationUser({
      userId: data.userId,
      associationId: data.associationId,
    });

    if (exists) {
      return this.prisma.associationsUsers.update({
        where: {
          userId_associationId: {
            associationId: data.associationId,
            userId: data.userId,
          },
        },
        data: {
          role: data.role,
        },
      });
    }

    return this.prisma.associationsUsers.create({
      data,
    });
  }

  async updateAssociationUser(
    where: UpdateOrDeleteAssociationUserParams,
    newData: Prisma.AssociationsUsersUpdateInput,
  ) {
    return this.prisma.associationsUsers.update({
      where: {
        userId_associationId: where,
      },
      data: newData,
    });
  }

  async deleteAssociationUser(data: UpdateOrDeleteAssociationUserParams) {
    return this.prisma.associationsUsers.delete({
      where: {
        userId_associationId: {
          associationId: data.associationId,
          userId: data.userId,
        },
      },
    });
  }
}
