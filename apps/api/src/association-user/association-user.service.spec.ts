import { Test, TestingModule } from '@nestjs/testing';
import { AssociationsUsers } from 'prisma-types';
import { PrismaService } from 'src/prisma/prisma.service';
import { AssociationUserService } from './association-user.service';

jest.setTimeout(30000);

const associationId = 'cl0v23pfz000c09l9d3bzdbgj';
const userId = 'cl0ux4rjh000009l034o3be5z';
const associationUserArray: AssociationsUsers[] = [
  {
    role: 'MEMBER',
    userId,
    associationId,
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    role: 'PRESIDENT',
    userId: 'cl0uxknxu000109jjc32g61x4',
    associationId,
    createdAt: new Date(),
    updatedAt: new Date(),
  },
];
const oneAssociationUser = associationUserArray[0];
const db = {
  associationsUsers: {
    findMany: jest.fn().mockResolvedValue(associationUserArray),
    findFirst: jest.fn().mockResolvedValue(oneAssociationUser),
    create: jest.fn().mockReturnValue(oneAssociationUser),
    update: jest.fn().mockResolvedValue(oneAssociationUser),
    delete: jest.fn().mockResolvedValue(oneAssociationUser),
  },
};

describe('AssociationUserService', () => {
  let service: AssociationUserService;
  let prisma: PrismaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AssociationUserService,
        {
          provide: PrismaService,
          useValue: db,
        },
      ],
    }).compile();

    service = module.get<AssociationUserService>(AssociationUserService);
    prisma = module.get<PrismaService>(PrismaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getAssociationUsers', () => {
    it('should get all users of the association', () => {
      expect(service.getAssociationUsers({ associationId })).resolves.toEqual(
        associationUserArray,
      );
    });
  });

  describe('getAssociationUser', () => {
    it('should get an association user', () => {
      expect(
        service.getAssociationUser({ associationId, userId }),
      ).resolves.toEqual(oneAssociationUser);
    });
  });

  describe('createAssociationUser', () => {
    it('should create an association user', () => {
      expect(
        service.createAssociationUser({
          role: 'MEMBER',
          userId,
          associationId,
        }),
      ).resolves.toEqual(oneAssociationUser);
    });
  });

  describe('updateAssociationUser', () => {
    it('should update the associationUser', () => {
      expect(
        service.updateAssociationUser(
          { associationId, userId },
          { role: 'MEMBER' },
        ),
      ).resolves.toEqual(oneAssociationUser);
    });
  });
  describe('deleteAssociationUser', () => {
    it('should delete the association user', () => {
      expect(
        service.deleteAssociationUser({ associationId, userId }),
      ).resolves.toEqual(oneAssociationUser);
    });
  });
});
