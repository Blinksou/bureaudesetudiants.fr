import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaModule } from './prisma/prisma.module';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AssociationModule } from './association/association.module';
import { APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { JwtAuthGuard } from './auth/jwt/guard';
import { UserProfileModule } from './user-profile/user-profile.module';
import { MulterModule } from '@nestjs/platform-express';
import Joi from 'joi';
import { SentryModule, SentryInterceptor } from '@ntegral/nestjs-sentry';
import { AssociationUserModule } from './association-user/association-user.module';
import { NewsModule } from './news/news.module';
import { EventsModule } from './events/events.module';
import config from 'config/config';

@Module({
  imports: [
    SentryModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => {
        const sentryDsn = config.get<string>('SENTRY_DSN');

        return {
          dsn: sentryDsn,
          debug: true,
          environment: 'development',
          enabled: !!sentryDsn,
          tracesSampleRate: 1.0,
        };
      },
      inject: [ConfigService],
    }),
    ConfigModule.forRoot({
      isGlobal: true,
      validationSchema: Joi.object({
        UPLOADED_FILES_DESTINATION: Joi.string().required(),
        SENTRY_DSN: Joi.string().required(),
      }),
      load: [config],
      expandVariables: true,
    }),
    MulterModule.register({
      dest: '../uploads',
    }),
    PrismaModule,
    UserModule,
    AuthModule,
    AssociationModule,
    UserProfileModule,
    AssociationUserModule,
    NewsModule,
    EventsModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
    {
      provide: APP_INTERCEPTOR,
      useFactory: () => new SentryInterceptor(),
    },
  ],
})
export class AppModule {}
