import {
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Request,
  Res,
} from '@nestjs/common';
import { NewsService } from './news.service';
import { CreateNewsDto } from './dto/create-news.dto';
import { UpdateNewsDto } from './dto/update-news.dto';
import { RequestWithUser } from '../../types/request';
import { AssociationUserService } from '../association-user/association-user.service';
import { Role } from 'prisma-types';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';

@ApiTags('News')
@Controller()
export class NewsController {
  constructor(
    private readonly newsService: NewsService,
    private readonly associationUserService: AssociationUserService,
  ) {}

  @Post('associations/:associationId/news')
  @ApiResponse({ status: 201 })
  @ApiResponse({ status: 401 })
  async create(
    @Request() req: RequestWithUser,
    @Param('associationId') associationId: string,
    @Body() createNewsDto: CreateNewsDto,
  ) {
    const member = await this.associationUserService.getAssociationUser({
      associationId,
      userId: req.user.id,
    });

    const allowedRoles: Array<Role> = [
      Role.PRESIDENT,
      Role.COMMUNITY_MANAGER,
      Role.EVENT_MANAGER,
    ];

    if ((!member || !allowedRoles.includes(member.role)) && !req.user.isAdmin) {
      throw new ForbiddenException();
    }

    return this.newsService.create(createNewsDto, req.user, associationId);
  }

  @Get('news')
  async findAll() {
    return this.newsService.findAll();
  }

  @Get('associations/:associationId/news')
  async findAllByAssociation(@Param('associationId') associationId: string) {
    return this.newsService.findAllByAssociationId(associationId);
  }

  @Get('news/:id')
  @ApiResponse({ status: 200 })
  async findOne(@Param('id') id: string) {
    return this.newsService.findOne(id);
  }

  @Patch('news/:id')
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 401 })
  async update(
    @Request() req: RequestWithUser,
    @Param('id') id: string,
    @Body() updateNewsDto: UpdateNewsDto,
  ) {
    const article = await this.newsService.findOne(id);

    if (!article) {
      throw new NotFoundException();
    }

    const member = await this.associationUserService.getAssociationUser({
      associationId: article.associationId,
      userId: req.user.id,
    });

    const allowedRoles: Array<Role> = [
      Role.PRESIDENT,
      Role.COMMUNITY_MANAGER,
      Role.EVENT_MANAGER,
    ];

    if ((!member || !allowedRoles.includes(member.role)) && !req.user.isAdmin) {
      throw new ForbiddenException();
    }

    return this.newsService.update(id, updateNewsDto);
  }

  @Delete('news/:id')
  @ApiResponse({ status: 204 })
  @ApiResponse({ status: 401 })
  async remove(
    @Res() res: Response,
    @Request() req: RequestWithUser,
    @Param('id') id: string,
  ) {
    const article = await this.newsService.findOne(id);

    if (!article) {
      throw new NotFoundException();
    }

    const member = await this.associationUserService.getAssociationUser({
      associationId: article.associationId,
      userId: req.user.id,
    });

    const allowedRoles: Array<Role> = [
      Role.PRESIDENT,
      Role.COMMUNITY_MANAGER,
      Role.EVENT_MANAGER,
    ];

    if ((!member || !allowedRoles.includes(member.role)) && !req.user.isAdmin) {
      throw new ForbiddenException();
    }

    await this.newsService.remove(id);

    return res.status(204);
  }
}
