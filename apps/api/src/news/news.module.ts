import { Module } from '@nestjs/common';
import { NewsService } from './news.service';
import { NewsController } from './news.controller';
import { AssociationUserModule } from '../association-user/association-user.module';

@Module({
  controllers: [NewsController],
  providers: [NewsService],
  imports: [AssociationUserModule],
})
export class NewsModule {}
