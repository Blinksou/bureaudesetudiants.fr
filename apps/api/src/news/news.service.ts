import { Injectable } from '@nestjs/common';
import { CreateNewsDto } from './dto/create-news.dto';
import { UpdateNewsDto } from './dto/update-news.dto';
import { PrismaService } from '../prisma/prisma.service';
import { User } from 'prisma-types';

@Injectable()
export class NewsService {
  constructor(private readonly prisma: PrismaService) {}

  async create(
    createNewsDto: CreateNewsDto,
    user: User,
    associationId: string,
  ) {
    return this.prisma.news.create({
      data: {
        ...createNewsDto,
        userId: user.id,
        associationId,
      },
    });
  }

  async findAll() {
    return this.prisma.news.findMany({});
  }

  async findAllByAssociationId(id: string) {
    return this.prisma.news.findMany({ where: { associationId: id } });
  }

  async findOne(id: string) {
    return this.prisma.news.findFirst({
      where: {
        id,
      },
    });
  }

  async update(id: string, updateNewsDto: UpdateNewsDto) {
    return this.prisma.news.update({ where: { id }, data: updateNewsDto });
  }

  async remove(id: string) {
    return this.prisma.news.delete({ where: { id } });
  }
}
