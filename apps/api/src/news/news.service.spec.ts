import { Test, TestingModule } from '@nestjs/testing';
import { NewsService } from './news.service';
import { News, User } from 'prisma-types';
import { PrismaService } from 'src/prisma/prisma.service';

const user: User = {
  id: 'cl0ux4rjh000009l034o3be5z',
  email: 'hello@world.com',
  password: 'mysuperpass',
  isAdmin: false,
  createdAt: new Date(),
  updatedAt: new Date(),
};
const newsArray: News[] = [
  {
    id: 'a super cuid',
    title: 'an amazing title',
    content: 'a really good content',
    userId: 'userid',
    associationId: 'assoid',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    id: 'a super cuid 2',
    title: 'an amazing title 2',
    content: 'a really good content 2',
    userId: 'userid2',
    associationId: 'assoid2',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
];
const oneNews = newsArray[0];
const db = {
  news: {
    create: jest.fn().mockReturnValue(oneNews),
    findMany: jest.fn().mockResolvedValue(newsArray),
    findFirst: jest.fn().mockResolvedValue(oneNews),
    update: jest.fn().mockResolvedValue(oneNews),
    delete: jest.fn().mockResolvedValue(oneNews),
  },
};

describe('NewsService', () => {
  let service: NewsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NewsService,
        {
          provide: PrismaService,
          useValue: db,
        },
      ],
    }).compile();

    service = module.get<NewsService>(NewsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create', () => {
    it('should create a news', async () => {
      const createdNews = await service.create(
        {
          title: 'a title',
          content: 'content',
        },
        user,
        'asso id',
      );
      expect(createdNews).toEqual(oneNews);
    });
  });

  describe('findAll', () => {
    it('should return all news', () => {
      expect(service.findAll()).resolves.toEqual(newsArray);
    });
  });

  describe('findAllByAssociationId', () => {
    it('should return all news', () => {
      expect(service.findAllByAssociationId('asso id')).resolves.toEqual(
        newsArray,
      );
    });
  });

  describe('findOne', () => {
    it('should return all news', () => {
      expect(service.findOne('id')).resolves.toEqual(oneNews);
    });
  });

  describe('update', () => {
    it('should update a news', () => {
      expect(service.update('an id', { content: 'fbiebf' })).resolves.toEqual(
        oneNews,
      );
    });
  });

  describe('remove', () => {
    it('should remove a news', () => {
      expect(service.remove('an id')).resolves.toEqual(oneNews);
    });
  });
});
