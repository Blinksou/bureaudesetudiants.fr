import { Prisma } from 'prisma-types';

export const user: Prisma.UserCreateInput[] = [
  {
    id: 'cl0ux4rjh000009l034o3be5z',
    email: 'admin@admin.com',
    password:
      '$argon2i$v=19$m=4096,t=3,p=1$vnG6qcxwRj3aYU6f59DuKg$9OsOfLhRKX88V8+unNiIpKDGcGgJHWj+afmaH5WVEuA', // password
    isAdmin: true,
    profile: {
      create: {
        firstname: 'Marvyn',
        lastname: 'Aboulycam',
        phone: '0612345678',
      },
    },
  },
  {
    id: 'cl0uxknxu000109jjc32g61x4',
    email: 'alex@ynov.com',
    password:
      '$argon2i$v=19$m=4096,t=3,p=1$vnG6qcxwRj3aYU6f59DuKg$9OsOfLhRKX88V8+unNiIpKDGcGgJHWj+afmaH5WVEuA', // password
    isAdmin: false,
    profile: {
      create: {
        firstname: 'Alex',
        lastname: 'Boisseau',
        phone: '0612345678',
      },
    },
  },
  {
    id: 'cl0v21x5d000a09l9crbbbfad',
    email: 'thomas@ynov.com',
    password:
      '$argon2i$v=19$m=4096,t=3,p=1$vnG6qcxwRj3aYU6f59DuKg$9OsOfLhRKX88V8+unNiIpKDGcGgJHWj+afmaH5WVEuA', // password
    isAdmin: false,
    profile: {
      create: {
        firstname: 'Thomas',
        lastname: 'Ln',
        phone: '0612345678',
      },
    },
  },
  {
    id: 'cl0uxgg1o000009jjeual2sj1',
    email: 'user@user.com',
    password:
      '$argon2i$v=19$m=4096,t=3,p=1$vnG6qcxwRj3aYU6f59DuKg$9OsOfLhRKX88V8+unNiIpKDGcGgJHWj+afmaH5WVEuA', // password
    isAdmin: false,
    profile: {
      create: {
        firstname: 'Alex',
        lastname: 'Boisseau',
        phone: '0612345678',
      },
    },
  },
  {
    id: 'cl0v1vb5o000009l9a746c9ow',
    email: 'user2@user2.com',
    password:
      '$argon2i$v=19$m=4096,t=3,p=1$vnG6qcxwRj3aYU6f59DuKg$9OsOfLhRKX88V8+unNiIpKDGcGgJHWj+afmaH5WVEuA', // password
    isAdmin: false,
    profile: {
      create: {
        firstname: 'Ali',
        lastname: 'Nabi',
        phone: '0612345678',
      },
    },
  },
];
