import { Prisma } from 'prisma-types';

export const association: Prisma.AssociationCreateInput[] = [
  {
    id: 'cl0v23pfz000c09l9d3bzdbgj',
    name: 'Ma super asso',
    description: 'Hello World',
    address1: 'Hi',
    city: 'Bordeaux',
    zipCode: '33000',
    country: 'France',
    status: 'VALIDATE',
    users: {
      createMany: {
        data: [
          {
            role: 'PRESIDENT',
            userId: 'cl0uxknxu000109jjc32g61x4',
          },
          {
            role: 'MEMBER',
            userId: 'cl0uxgg1o000009jjeual2sj1',
          },
        ],
      },
    },
  },
  {
    id: 'cl0v27b4n000g09l93za84j0e',
    name: 'Ynov asso',
    description: 'Hello World',
    address1: 'Hi',
    city: 'Bordeaux',
    zipCode: '33000',
    country: 'France',
    users: {
      createMany: {
        data: [
          {
            role: 'PRESIDENT',
            userId: 'cl0v21x5d000a09l9crbbbfad',
          },
          {
            role: 'MEMBER',
            userId: 'cl0v1vb5o000009l9a746c9ow',
          },
        ],
      },
    },
  },
];
