import { Prisma } from 'prisma-types';

export const news: Prisma.NewsUncheckedCreateInput[] = [
  {
    id: 'cl152xotl000109mlczlj0rgw',
    title: 'Forgotten Birch',
    content:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In fringilla tincidunt quam non efficitur. Sed convallis nisi et posuere vulputate. Praesent a massa ac eros auctor viverra. Nunc porta tellus massa, at hendrerit ante venenatis quis. Vivamus placerat massa lorem, nec viverra dolor finibus a. Ut auctor felis non massa.',
    userId: 'cl0uxknxu000109jjc32g61x4',
    associationId: 'cl0v23pfz000c09l9d3bzdbgj',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    id: 'cl152y47k000209ml4vs1gsqv',
    title: 'The Red Ice',
    content:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a vulputate ipsum. Nunc nec orci egestas, eleifend mi eu, interdum eros. Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse enim metus, eleifend vitae mauris id, pharetra finibus ipsum. Mauris ipsum lorem, placerat nec neque ut, pretium varius.',
    userId: 'cl0v1vb5o000009l9a746c9ow',
    associationId: 'cl0v27b4n000g09l93za84j0e',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    id: 'cl153cikr000009l8782zfmf1',
    title: "The Word's Scent",
    content:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a vulputate ipsum. Nunc nec orci egestas, eleifend mi eu, interdum eros. Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse enim metus, eleifend vitae mauris id, pharetra finibus ipsum. Mauris ipsum lorem, placerat nec neque ut, pretium varius.',
    userId: 'cl0v1vb5o000009l9a746c9ow',
    associationId: 'cl0v27b4n000g09l93za84j0e',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    id: 'cl153drlr000109l8am22awkf',
    title: 'Wizards in the Vision',
    content:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In fringilla tincidunt quam non efficitur. Sed convallis nisi et posuere vulputate. Praesent a massa ac eros auctor viverra. Nunc porta tellus massa, at hendrerit ante venenatis quis. Vivamus placerat massa lorem, nec viverra dolor finibus a. Ut auctor felis non massa.',
    userId: 'cl0uxknxu000109jjc32g61x4',
    associationId: 'cl0v23pfz000c09l9d3bzdbgj',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
];
