import { Prisma } from 'prisma-types';

export const event: Prisma.EventCreateInput[] = [
  {
    id: 'cl155j67f000009l313hcd2kk',
    name: 'The Entwined Prince',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam finibus a mi quis consectetur. Pellentesque sem metus, faucibus id congue vitae, viverra vel nisi. Nam fringilla arcu vel lorem posuere venenatis. In nibh nulla, bibendum.',
    isPublic: false,
    startDate: new Date(),
    association: {
      connect: {
        id: 'cl0v23pfz000c09l9d3bzdbgj',
      },
    },
  },
  {
    id: 'cl156aijy00000ajp53jb1loq',
    name: 'Mists of Past',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sagittis vel metus a ornare. Aenean dapibus mauris sed lectus facilisis sodales. Nullam ac diam ac ex vestibulum volutpat et in tortor. Phasellus quis velit fermentum.',
    isPublic: false,
    startDate: new Date(),
    association: {
      connect: {
        id: 'cl0v23pfz000c09l9d3bzdbgj',
      },
    },
  },
  {
    id: 'cl156bx5m000009jq804uh4sq',
    name: 'The Voyages of the Mist',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent efficitur turpis id dui porttitor, eget cursus eros convallis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed sollicitudin, enim vitae.',
    isPublic: false,
    startDate: new Date(),
    association: {
      connect: {
        id: 'cl0v27b4n000g09l93za84j0e',
      },
    },
  },
];
