import { Prisma, PrismaClient } from 'prisma-types';

export async function clearDb(prisma: PrismaClient) {
  const tables = Prisma.dmmf.datamodel.models
    .map((model) => model.name)
    .filter((table) => table);

  await prisma.$transaction([
    ...tables.map((table) =>
      prisma.$executeRawUnsafe(`TRUNCATE TABLE "${table}" CASCADE;`),
    ),
  ]);
}
