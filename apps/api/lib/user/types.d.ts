import { Profile, User } from 'prisma-types';

export type UserWithProfile = User & { profile: Profile };
